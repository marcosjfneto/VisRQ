<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OutroDiagrama extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outros_diagramas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_componente_pai')->unsigned();
            $table->integer('id_componente_irmao')->unsigned();
            $table->string('titulo_diagrama', 60);
            $table->string('nome_imagem', 100);
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outros_diagramas');
    }
}
