@extends('layouts.app')

@section('conteudo')

<!--
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.1/angular.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  -->


<script>
$(document).ready(function(){
    var metade_div_quad = 25;
    $("#main-wrapper").dblclick(function(event){//mousemove(function(event){//#div_pai
           $("[name='pos_x']").val(event.pageX ); 
           $("[name='pos_y']").val(event.pageY); 
    });
});
</script>

 
<!--ESTILO USADO PARA TESTE--> 

<div id="div_pai" > 
    <?php
//    if( file_exists ( base_path() . '/public/imagens/'. $arquitetura[0]->nome_imagem ) ){
        echo '<b> '. $arquitetura[0]->titulo_grupo .' </b><br/><br/> ';
        echo '<div id="div_img_arquitetura"  > <img id="img_arquitetura" style=" z-index:1; " src="/imagens/'. $arquitetura[0]->nome_imagem .'" /> </div> <br/><br/> ';
//    }  width: 100%; height: 100%;
    ?>
    
    <!--text-right-->
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group no-margin-hr ">
                <a id="bt_outro_diagrama" class="btn btn-success" ng-click="form_outro_diagrama()">
                    <span ng-show="!outro_diagrama"> Outro diagrama </span>
                    <span ng-show="outro_diagrama"> Grupo </span>
                </a>
            </div>
        </div>
    </div>
    
    <form action="{{ route('arquitetura.importar_outro_diagrama_post') }}" id="my_form" 
          method="POST" class="panel form-horizontal" enctype="multipart/form-data" ng-show="outro_diagrama">
        <div class="panel-heading">
            <span class="panel-title">Importar outro diagrama</span>
        </div>
          
        <div class="panel-body">
          <div class="row col-sm-6">
            <div class="row">
                <div class="col-sm-11">
                    <div class="form-group no-margin-hr" >
                        <label class="control-label">Título do diagrama</label>
                        <input type="text" name="titulo_diagrama" class="form-control"  ng-model="titulo_diagrama" required>
                    </div>
                </div><!-- col-sm-6 -->
            </div><!-- row -->
            
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group no-margin-hr">
                        <label class="control-label">Diagrama detalhado do componente</label>
                        <input type="file" name="diagrama" ng-model="diagrama">
                    </div>
                </div><!-- col-sm-6 -->
            </div>
            
            
            <input type="hidden" name="id_componente_pai" value="{{ $arquitetura[0]->id_componente_pai }}">
            <input type="hidden" name="id_componente_irmao" value="{{ $arquitetura[0]->id_componente_irmao }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
          
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group no-margin-hr  text-right">
                        <a id="bt_cancelar_outro_diagrama" onclick="" class="btn btn-default" >Cancelar</a>
                        <button class="btn btn-primary" >Salvar</button>
                        <!--ng-click="SendData()"-->
                    </div>
                </div><!-- col-sm-6 -->
            </div><!-- row -->
        </div>
      </div>
    </form>   
            
    
<!--TODO FAZER COM ANGULAR-->
<form action="{{ route('arquitetura.store_componente') }}" id="my_form" method="POST" class="panel form-horizontal" enctype="multipart/form-data" ng-show="!outro_diagrama">
        <div class="panel-heading">
            <span class="panel-title">Criar/Editar Grupo</span>
        </div>
          
        <div class="panel-body">
          <div class="row col-sm-6">
            <div class="row">
                <div class="col-sm-11">
                    <div class="form-group no-margin-hr" >
                        <label class="control-label">Título do grupo</label>
                        <input type="text" name="titulo_grupo" class="form-control"  ng-model="titulo_grupo" required>
                    </div>
                </div><!-- col-sm-6 -->
            </div><!-- row -->
            
            <div class="row">
                <div class="col-sm-11">
                    <div class="form-group no-margin-hr">
                        <label class="control-label">Pacote do componente</label>
                        <input type="text" name="pacote_componente" ng-model="pacote_componente" class="form-control">
                    </div>
                </div><!-- col-sm-6 -->
            </div>
           
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group no-margin-hr">
                        <label class="control-label">Diagrama detalhado do componente</label>
                        <input type="file" name="diagrama_componentes" ng-model="diagrama_componentes">
                    </div>
                </div><!-- col-sm-6 -->
            </div>
            
            
             <div class="row">
                <div class="col-sm-12">
                    <label class="control-label">Possicionamento do marcador (centro)</label>
                    <div class="form-group no-margin-hr">
                        <!--<span id="xy"></span>-->
                        
                        
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group no-margin-hr">
                                    <label class="control-label">X</label>
                                    <input type="text" name="pos_x" class="form-control" ng-model="pos_x">
                                </div>
                            </div><!-- col-sm-6 -->
                            <div class="col-sm-2">
                                <div class="form-group no-margin-hr">
                                    <label class="control-label">Y</label>
                                    <input type="text" name="pos_y"  class="form-control" ng-model="pos_y">
                                </div>
                            </div><!-- col-sm-6 -->
                        </div><!-- row -->
                    </div>
                </div><!-- col-sm-6 -->
            </div>
            
          
            <input type="hidden" name="id_componente_pai" value="{{ $arquitetura[0]->id }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <input type="hidden" name="dese_p1" value="">
            <input type="hidden" name="dese_p2" value="">
            <input type="hidden" name="conf_p1" value="">
            <input type="hidden" name="conf_p2" value="">
            <!--<input type="hidden" name="id" value="">-->

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group no-margin-hr  text-right">
                        <a id="bt_cancelar" onclick="cancelar()" class="btn btn-default" >Cancelar</a>
                        <button class="btn btn-primary" >Salvar</button>
                        <!--ng-click="SendData()"-->
                    </div>
                </div><!-- col-sm-6 -->
            </div><!-- row -->
        </div>
            
       <div class="row col-sm-4">  
            <!-- 6. $RANGE_SLIDERS =============================================================================-->
               
        
            <div class="text-bg"><span class="text-slim">Requisitos</span> </div>
            </br>
            <div class="checkbox" style="margin: 0;">
                <label>
                    <!--<input type="checkbox" value="" class="px">-->
                    <span class="lbl">Confiabilidade</span>
                    <select name="confiabilidade_escala">
                        <option value="1">1</option> 
                        <option value="10">10</option>
                        <option value="100">100</option>
                        <option value="1000">1k</option>
                        <option value="10000">10k</option>
                    </select>
                </label>

                </br>
                        <!-- Horizontal sliders -->
        <!--                <div class="ui-slider-range-demo"></div>
                        <div class="ui-slider-success ui-slider-range-demo"></div>
                        <div class="ui-slider-danger ui-slider-range-demo"></div>
                        <div class="ui-slider-info ui-slider-range-demo"></div>-->
                <div id="atr_confiabilidade" class="ui-slider-warning ui-slider-range-demo"></div>
                      
                <div class="row ">
<!--                    <label name="_conf_p1" class="col-sm-6 control-label">P1: 25</label>
                    <label name="_conf_p2" class="col-sm-6 control-label">P2: 50</label>-->
                     <input type="text" name="_conf_p1" class="col-sm-3" disabled="">
                    <input type="text" name="_conf_p2" class="col-sm-3" disabled="">
                </div>

                </br>
                <label>
                    <!--<input type="checkbox" value="" class="px">-->
                    <span class="lbl">Desempenho</span>
                    <select name="desempenho_escala">
                        <option value="1">1</option> 
                        <option value="0.1">0.1</option>
                        <option value="0.01">0.01</option>
                        <option value="0.001">0.001</option>
                        <option value="0.0001">0.0001</option>
                        <option value="0.00001">0.00001</option>
                    </select>
                </label>
                </br>
                <div id="atr_desempenho" class="ui-slider-warning ui-slider-range-demo"></div>
                
                <div class="row form-group">
                    <!--<label name="_dese_p1" class="col-sm-6 control-label">P1: 25</label>-->
                    <!--<label name="_dese_p2" class="col-sm-6 control-label">P2: 50</label>-->
                   <input type="text" name="_dese_p1" class=" col-sm-3" disabled="">
                   <input type="text" name="_dese_p2" class=" col-sm-3" disabled="">
                </div>
            </div> 
        </div>
                <!-- /6. $RANGE_SLIDERS -->

        </br></br>
        </div>    
    </form>

    <!--BAIXAR OS ARQUIVOS .JS E EXECUTAR-->
                <style>
                        .ui-slider-range-demo { margin-bottom: 20px; }
                        .ui-v-slider-range-demo { float: left; margin-right: 20px; }
                        .right-to-left .ui-v-slider-range-demo { float: right; margin-left: 20px; margin-right: 0; }
                </style>

                <script>
                        init.push(function () {
                                var range_sliders_options = {
                                        'range': true,
                                        'min': 0,
                                        'max': 100,
                                        'values': [ 30, 60 ]
                                };
                                $('.ui-slider-range-demo').slider(range_sliders_options);
        //			$('.ui-v-slider-range-demo').slider($.extend({ orientation: 'vertical' }, range_sliders_options));
                        });
                
                            $( "#atr_confiabilidade" ).on( "slidechange", function( event, ui ) {
                                var intervalo = $( "#atr_confiabilidade" ).slider( "option", "values" );
                                $("[name='_conf_p1']").val( "P1: "+intervalo[0] ); 
                                $("[name='_conf_p2']").val( "P2: "+intervalo[1] );
                                
                                $("[name='conf_p1']").val( intervalo[0] ); 
                                $("[name='conf_p2']").val( intervalo[1] );
        //                       http://tableless.com.br/manipulacao-de-classes-com-jquery/ - alterar as classes dos componentes
                            } );
                       
                    $( "#atr_desempenho" ).on( "slidechange", function( event, ui ) {
                        var intervalo = $( "#atr_desempenho" ).slider( "option", "values" );
                        $("[name='_dese_p1']").val( "P1: "+ intervalo[0] ); 
                        $("[name='_dese_p2']").val( "P2: "+intervalo[1] ); 
                        
                        $("[name='dese_p1']").val( intervalo[0] ); 
                        $("[name='dese_p2']").val( intervalo[1] ); 
                     });
                     
                </script>
    
    
<script>

$("#bt_cancelar").hide();
    
function set_informacoes(id) {
//INTEGRAÇÃO ANGULAR COM JAVASCRIPT
    var itens = angular.element('[ng-controller="appController"]').scope().componentes;
    var item = itens[id];
//    var item = "";
//    var i = 0;
//    for(i = 0; i < itens.length; i++){
//        if(itens[i].id == id){
//            item = itens[i];
//        } 
//    }
//    
//    $("[name='myform']").action('teste');
//    var end = " route('arquitetura.update',['id'=>"+id+"]) ";
    var end = "http://localhost:8000/arquitetura/editar/"+item.id;

    $('#my_form').attr('action', end);
    $("[name='titulo_grupo']").val(item.titulo_grupo); 
    $("[name='pacote_componente']").val(item.pacote_componente); 
    $("[name='id']").val(item.id);
    $("[name='pos_x']").val(item.pos_x); 
    $("[name='pos_y']").val(item.pos_y); 
    
    $("[name='_conf_p1']").val( "P1: "+ item.confiabilidade_p1 ); 
    $("[name='_conf_p2']").val( "P2: "+ item.confiabilidade_p2 );
    $( "#atr_confiabilidade" ).slider( "values", [item.confiabilidade_p1, item.confiabilidade_p2] );
    
    $("[name='_dese_p1']").val( "P1: "+ item.desempenho_p1 ); 
    $("[name='_dese_p2']").val( "P2: "+ item.desempenho_p2 ); 
    $( "#atr_desempenho" ).slider( "values", [item.desempenho_p1, item.desempenho_p2] );
    
    //FALTA SETAR ESCALA
    
    $("#bt_cancelar").show();
    $(".text_box2").hide();
//        $("[name='pos_y']").val(s); 
//        $("[name='pacote_componente']").val(end); 

}
function cancelar(){
    $('#my_form').attr('action', "{{ route('arquitetura.store_componente') }}");
    $("[name='titulo_grupo']").val(""); 
    $("[name='pacote_componente']").val(""); 
    $("[name='id']").val("");
    $("[name='pos_x']").val(""); 
    $("[name='pos_y']").val(""); 
    
    
    $( "#atr_confiabilidade" ).slider( "values", [30, 60] );
    $( "#atr_desempenho" ).slider( "values", [30, 60] );
    
    $("[name='_conf_p1']").val( "" ); 
    $("[name='_conf_p2']").val( "" );
    $("[name='_dese_p1']").val( ""); 
    $("[name='_dese_p2']").val( ""); 
    
    $("#bt_cancelar").hide();
    $(".text_box2").show();
}

function teste(){
    $('#my_form').attr('action', "{{ route('arquitetura.store_componente') }}");
}

function mostrar(){
    alert($('#my_form').attr('action'));
}
</script>

<script>
    var app = angular.module('app', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    
	 
    app.controller('appController', function ($scope, $http, $sce) {
        $scope.componentes = <?= $componentes ?>;
        $scope.outro_diagrama = false;
        
        $scope.form_outro_diagrama = function() {
            $scope.outro_diagrama = !$scope.outro_diagrama;
        }
        
        $scope.desenha_elementos = function (componentes){
            var _html = "";
            var metade_div_quad = 20;
            var i; 
            var item;
            for(i = 0; i < componentes.length; i++){
                item = componentes[i];
//                _html +=  "<div class='text_box' style='position: absolute;"; //utilizado para todos os atributos
                _html +=  "<div class='text_box2' style='position: absolute;";
                _html += "left: "+(item.pos_x-metade_div_quad)+"px; ";
                _html += "top: " +(item.pos_y-metade_div_quad)+"px; z-index:10;' > ";
//              CONTEUDO
//                _html += '<button onclick="set_informacoes('+item.id+')"><i class="menu-icon fa fa-bell">?</i></button>';
                _html += '<a onclick="set_informacoes('+i+')">';
                _html +=    '<i class="btn-rounded btn-primary btn-xs fa fa-edit "></i></a>';
                
                _html += '<a href="http://localhost:8000/arquitetura/excluir/'+item.id+'">';
                _html +=    '<i class="btn-rounded btn-danger btn-xs fa fa-trash-o "> </i></a>';
                
//                _html += "CONTEUDO ";
                
//                _html += '<button ng-click="myFunc()"'.'>Click Me!</button>';
                _html += "</div>";
            }
            
//            alert(_html);
            $scope.snippet = $sce.trustAsHtml(_html);
        };
        
        $scope.desenha_elementos($scope.componentes);
               
//        $scope.edita_componente = function (){
//            var i = 0;
//            alert(i);
//            alert($scope.componentes[i].id );
//            var len = $scope.componentes.length;
//            for (i; i < len; i++) {
//                if ($scope.componentes[i].id==id){
//                    alert($scope.componentes[i].id );
//                }
//            }
//        };
        
//        $scope.edita_componente(); 
    });
</script>

    
    <!--BAIXAR OS ARQUIVOS .JS E EXECUTAR-->
   



@endsection
