@extends('layouts.app')

@section('conteudo')
<div >
    
    <!-- 6. $HORIZONTAL_FORM ===========================================================================

                                Horizontal form
    -->
    <form action="{{ route('arquitetura.store') }}" method="POST" class="panel form-horizontal" enctype="multipart/form-data">
        <div class="panel-heading">
            <span class="panel-title">Arquitetura</span>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr">
                        <label class="control-label">Nome da figura</label>
                        <input type="nome" name="nome" class="form-control" required>
                    </div>
                </div><!-- col-sm-6 -->
            </div><!-- row -->

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr">
                        <label class="control-label">Imagens do problema</label>
                        </br>
                         <!--imagem-->
                         <!--<input type="hidden" name="MAX_FILE_SIZE" value="150000" />-->
                        <input type="file" name="arquitetura_principal">
                    </div>
                </div>
            </div>  
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr  text-right">
                        <button class="btn btn-primary">Salvar</button>
                    </div>
                </div><!-- col-sm-6 -->
            </div><!-- row -->
            
            
        </div>
    </form>

</div>
@endsection
