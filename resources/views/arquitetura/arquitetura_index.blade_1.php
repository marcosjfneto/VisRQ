@extends('layouts.app')
<!--TODO COLOCAR O ID COMO O "NOME DO PACOTE_ATRIBUTO MONITORADO" PARA USAR O JAVASCRIPT E JQUERY PARA ALTERA A COR DOS ELEMENTOS--> 
<!--TODO COLOCAR UMA ARVORE COM OS ELEMENTOS PARA FACILITAR A NAVEGAÇÃO ENTRE COMONENTES--> 
<!--slider é preciso ajustar escala  http://api.jqueryui.com/slider/-->

@if(count($arquitetura)==1)

@section('menu_requisitos')
 <!-- 6. $RANGE_SLIDERS =============================================================================-->
        <!-- Styles -->
        <style>
                .ui-slider-range-demo { margin-bottom: 20px; }
                .ui-v-slider-range-demo { float: left; margin-right: 20px; }
                .right-to-left .ui-v-slider-range-demo { float: right; margin-left: 20px; margin-right: 0; }
        </style>
        <!-- / Styles -->
        <!-- Javascript -->
        <script>
                init.push(function () {
                        var range_sliders_options = {
                                'range': true,
                                'min': 0,
                                'max': 100,
                                'values': [ 25, 50 ]
                        };
                        $('.ui-slider-range-demo').slider(range_sliders_options);
//			$('.ui-v-slider-range-demo').slider($.extend({ orientation: 'vertical' }, range_sliders_options));
                });
        </script>
        <!-- / Javascript -->
        
<div class="menu-content">
    <div class="text-bg"><span class="text-slim">Requisitos</span> </div>
    </br>

    <div class="checkbox" style="margin: 0;">
        <label>
            <input type="checkbox" value="" class="px">
            <span class="lbl">Confiabilidade</span>
            <select name="escala_confiabilidade">
                <option value="1">1</option> 
                <option value="10">10</option>
                <option value="100">100</option>
            </select>
        </label>
        
        </br>
        
        
                <!-- Horizontal sliders -->
<!--                <div class="ui-slider-range-demo"></div>
                <div class="ui-slider-success ui-slider-range-demo"></div>
                <div class="ui-slider-danger ui-slider-range-demo"></div>
                <div class="ui-slider-info ui-slider-range-demo"></div>-->
                <div id="atr_confiabilidade" class="ui-slider-warning ui-slider-range-demo"></div>
                <script>
                    $( "#atr_confiabilidade" ).on( "slidechange", function( event, ui ) {
                        var intervalo = $( "#atr_confiabilidade" ).slider( "option", "values" );
//                        alert(intervalo[0]+' ok '+intervalo[1]);
//                       http://tableless.com.br/manipulacao-de-classes-com-jquery/ - alterar as classes dos componentes
                        $("[name='conf_p1']").val( intervalo[0] ); 
                        $("[name='conf_p2']").val( intervalo[1] ); 
                     });
                </script>
                <div class="row form-group">
            <label name="conf_p1" class="col-sm-6 control-label">P1: 25</label>
<!--            <div class="col-sm-4">
                <input type="text" name="name" class="form-control">
            </div>-->
        
            <label name="conf_p2" class="col-sm-6 control-label">P2: 50</label>
<!--            <div class="col-sm-4">
                <input type="email" name="email" class="form-control">
            </div>-->
        </div>
                

        </br>
        <label>
            <input type="checkbox" value="" class="px">
            <span class="lbl">Desempenho</span>
            <select name="escala_confiabilidade">
                <option value="1">1</option> 
                <option value="0.1">0.1</option>
                <option value="0.01">0.01</option>
                <option value="0.001">0.001</option>
            </select>
        </label>
        </br>
        <div class="ui-slider-warning ui-slider-range-demo"></div>
        
        <div class="row form-group">
            <label name="dese_p1" class="col-sm-6 control-label">P1: 25</label>
<!--            <div class="col-sm-4">
                <input type="text" name="name" class="form-control">
            </div>-->
        
            <label name="dese_p2" class="col-sm-6 control-label">P2: 50</label>
<!--            <div class="col-sm-4">
                <input type="email" name="email" class="form-control">
            </div>-->
        </div>
    </div> 
</div>
        <!-- /6. $RANGE_SLIDERS -->

</br></br>
<!--<div class="menu-content">
    <div class="text-bg"><span class="text-slim">Requisitos</span> </div>
    </br>

    <tbody>
        <tr>

            <td>
                <div class="checkbox" style="margin: 0;">
                    <label>
                        <input type="checkbox" value="" class="px">
                        <span class="lbl">Confiabilidade</span>
                    </label>
                </div>

            </td>
            <td>
                <div class="checkbox" style="margin: 0;">
                    <label>
                        <input type="checkbox" value="" disabled="" class="px">
                        <span class="lbl">Desempenho</span>
                    </label>
                </div> 

            </td>
        </tr>
    </tbody>
</div>-->
@endsection
@endif

@section('conteudo')

<!--
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.1/angular.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  -->


<script>
$(document).ready(function(){
    var metade_div_quad = 25;

    $("#main-wrapper").dblclick(function(event){//mousemove(function(event){//#div_pai
           $("[name='pos_x']").val(event.pageX ); 
           $("[name='pos_y']").val(event.pageY); 
    });
});
</script>


@if(count($arquitetura)==1)
<div id="div_pai" > 
    <?php
        echo '<b> '. $arquitetura[0]->titulo_grupo .' </b><br/><br/> ';
        echo '<div id="div_img_arquitetura"> <img id="img_arquitetura" style=" z-index:1; " src="/imagens/'. $arquitetura[0]->nome_imagem .'" /> </div> <br/><br/> ';
    ?>
    <!--<a href="{{ route('arquitetura.edit',['id'=>$arquitetura[0]->id]) }}"> Editar </a>-->
    {{ $componentes }}
@else
    <form action="{{ route('arquitetura.store_arquitetura') }}" method="POST" class="panel form-horizontal" enctype="multipart/form-data">
        <div class="panel-heading">
            <span class="panel-title">Carregar diagrama principal</span>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr" >
                        <label class="control-label">Título</label>
                        <input type="text" name="titulo_grupo" class="form-control"  ng-model="titulo_grupo" required>
                    </div>
                </div>
            </div>
            
<!--            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr">
                        <label class="control-label">Pacote</label>
                        <input type="text" name="pacote_componente" ng-model="pacote_componente" class="form-control">
                    </div>
                </div>
            </div>-->
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr">
                        <label class="control-label">Diagrama</label>
                        <input type="file" name="arquitetura_principal">
                    </div>
                </div>
            </div>
            
            <input type="hidden" name="id" value="0">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr  text-right">
                        <button id='criarLinha' class="btn btn-primary" >Salvar</button>
                    </div>
                </div>
            </div>
       </div>     

    </form>
@endif
    
    <!--BAIXAR OS ARQUIVOS .JS E EXECUTAR-->
<!--TODO COLOCAR O ID COMO O "NOME DO PACOTE_ATRIBUTO MONITORADO" PARA USAR O JAVASCRIPT E JQUERY PARA ALTERA A COR DOS ELEMENTOS-->
<script>
    var app = angular.module('app', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    
    app.controller('appController', function ($scope, $http, $sce) {
        $scope.componentes = <?= $componentes ?>;
//        replicar na tela show
//        multiplicar pela escala
        $scope.seta_cor = function(p1, p2, escala, valor){
            if (valor < p1*escala){         //abaixo do limite
                return "btn-success";
            } else if (valor > p2*escala) { //acima do limite
                return "btn-danger";
            } else {                 //dentro do limite
                return "btn-warning";
            }    
        };
        
        $scope.desenha_elementos = function (componentes){
            var _html = "";
            var metade_div_quad = 20;
            var i; 
            var item;
            var valor = 5;
            
            for(i = 0; i < componentes.length; i++){
                item = componentes[i];
                
                _html +=  "<div class='text_box2' style='position: absolute;";
                _html += "left: "+(item.pos_x-metade_div_quad)+"px; ";
                _html += "top: " +(item.pos_y-metade_div_quad)+"px; z-index:10;' > ";
//              CONTEUDO


                if(item.nome_imagem != ""){
//                    _html += '<a href="http://localhost:8000/arquitetura/componente/'+item.id+'" >';
//                    _html += '<i class="btn-rounded btn-primary btn-xs fa fa-external-link"> </i></a>';
                    _html += '<a class="btn btn-xs btn-labeled btn-primary a_link" href="http://localhost:8000/arquitetura/componente/'+item.id+'" >';
                    _html += '<span class="btn-label icon fa fa-external-link a_link"> </span>';
                    _html += '</a>';
                } else{
                    _html += '<a class="btn btn-xs btn-labeled btn-default a_link">';
                    _html += '<span class="btn-label a_link"> </span>';
                    _html += '</a>';
                }
//                _html += '<a class="btn btn-xs btn-xs">';
//                _html += '<span class="btn-label icon fa fa-user"> </span>';
//                _html += '</a>';


//                TODO COLOCAR O ID COMO O "NOME DO PACOTE_ATRIBUTO MONITORADO" PARA USAR O JAVASCRIPT E JQUERY PARA ALTERA A COR DOS ELEMENTOS
//               replicar na tela show
                _html += '<a><i class="btn-rounded ';
                _html += $scope.seta_cor(item.desempenho_p1, item.desempenho_p2, item.desempenho_escala, item.avg_cpuTime); //para teste, colocar valor no lugar do artributo
                _html += ' btn-xs fa fa-bolt"> </i></a>';
                
                _html += '<a><i class="btn-rounded ';
                _html += $scope.seta_cor(item.confiabilidade_p1, item.confiabilidade_p2, item.confiabilidade_escala, item.qtd_exception);
                _html +=' btn-xs fa fa-check"> </i></a>';
//                
//                _html += '<a><i class="btn-rounded btn-danger btn-xs fa fa-bolt"> </i></a>';
//                _html += '<a><i class="btn-rounded btn-success btn-xs fa fa-check"> </i></a>';
//                
// OUTROS ATRIBUTOS
//                _html += '<a><i class="btn-rounded btn-warning btn-xs fa fa-user"> </i></a>';
//                _html += '<a><i class="btn-rounded btn-success btn-xs fa fa-th-large"> </i></a>';
//                _html += '<a><i class="btn-rounded btn-warning btn-xs fa fa-wrench"> </i></a>';
//                _html += '<a><i class="btn-rounded btn-danger btn-xs fa fa-cogs"> </i></a>';

                _html += "</div>";
            }
            
//          Inseri os elementos na tela
            $scope.snippet = $sce.trustAsHtml(_html);
        };
        
        $scope.desenha_elementos($scope.componentes);
    });
</script>

@endsection
