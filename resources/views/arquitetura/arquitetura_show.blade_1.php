@extends('layouts.app')

@section('conteudo')

@if(count($arquitetura)==1)

@section('menu_requisitos')
 <!-- 6. $RANGE_SLIDERS =============================================================================-->
        <!-- Styles -->
        <style>
                .ui-slider-range-demo { margin-bottom: 20px; }
                .ui-v-slider-range-demo { float: left; margin-right: 20px; }
                .right-to-left .ui-v-slider-range-demo { float: right; margin-left: 20px; margin-right: 0; }
        </style>
        <!-- / Styles -->
        <!-- Javascript -->
        <script>
                init.push(function () {
                        var range_sliders_options = {
                                'range': true,
                                'min': 0,
                                'max': 100,
                                'values': [ 25, 50 ]
                        };
                        $('.ui-slider-range-demo').slider(range_sliders_options);
//			$('.ui-v-slider-range-demo').slider($.extend({ orientation: 'vertical' }, range_sliders_options));
                });
        </script>
        <!-- / Javascript -->
        
<div class="menu-content">
    <div class="text-bg"><span class="text-slim">Requisitos</span> </div>
    </br>

    <div class="checkbox" style="margin: 0;">
        <label>
            <input type="checkbox" value="" class="px">
            <span class="lbl">Confiabilidade</span>
            <select name="escala_confiabilidade">
                <option value="1">1</option> 
                <option value="10">10</option>
                <option value="100">100</option>
            </select>
        </label>
        
        </br>
                <!-- Horizontal sliders -->
<!--                <div class="ui-slider-range-demo"></div>
                <div class="ui-slider-success ui-slider-range-demo"></div>
                <div class="ui-slider-danger ui-slider-range-demo"></div>
                <div class="ui-slider-info ui-slider-range-demo"></div>-->
                <div id="atr_confiabilidade" class="ui-slider-warning ui-slider-range-demo"></div>
                <script>
                    $( "#atr_confiabilidade" ).on( "slidechange", function( event, ui ) {
                        var intervalo = $( "#atr_confiabilidade" ).slider( "option", "values" );
                        alert(intervalo[0]+' ok '+intervalo[1]);
//                       http://tableless.com.br/manipulacao-de-classes-com-jquery/ - alterar as classes dos componentes
                    } );
                </script>
                <div class="row form-group">
            <label class="col-sm-6 control-label">P1: 25</label>
<!--            <div class="col-sm-4">
                <input type="text" name="name" class="form-control">
            </div>-->
        
            <label class="col-sm-6 control-label">P2: 50</label>
<!--            <div class="col-sm-4">
                <input type="email" name="email" class="form-control">
            </div>-->
        </div>
                

        </br>
        <label>
            <input type="checkbox" value="" class="px">
            <span class="lbl">Desempenho</span>
            <select name="escala_confiabilidade">
                <option value="1">1</option> 
                <option value="0.1">0.1</option>
                <option value="0.01">0.01</option>
                <option value="0.001">0.001</option>
            </select>
        </label>
        </br>
        <div class="ui-slider-warning ui-slider-range-demo"></div>
        
        <div class="row form-group">
            <label class="col-sm-6 control-label">P1: 25</label>
<!--            <div class="col-sm-4">
                <input type="text" name="name" class="form-control">
            </div>-->
        
            <label class="col-sm-6 control-label">P2: 50</label>
<!--            <div class="col-sm-4">
                <input type="email" name="email" class="form-control">
            </div>-->
        </div>
    </div> 
</div>
        <!-- /6. $RANGE_SLIDERS -->

</br></br>
<!--<div class="menu-content">
    <div class="text-bg"><span class="text-slim">Requisitos</span> </div>
    </br>

    <tbody>
        <tr>

            <td>
                <div class="checkbox" style="margin: 0;">
                    <label>
                        <input type="checkbox" value="" class="px">
                        <span class="lbl">Confiabilidade</span>
                    </label>
                </div>

            </td>
            <td>
                <div class="checkbox" style="margin: 0;">
                    <label>
                        <input type="checkbox" value="" disabled="" class="px">
                        <span class="lbl">Desempenho</span>
                    </label>
                </div> 

            </td>
        </tr>
    </tbody>
</div>-->
@endsection
@endif
  

@if(count($arquitetura)==1)

<div id="div_pai" > 
        
  
    <?php
//    if( file_exists ( base_path() . '/public/imagens/'. $arquitetura[0]->nome_imagem ) ){
        echo '<b>'. $arquitetura[0]->titulo_grupo .'</b><br/><br/> ';
        echo ' <div id="div_img_arquitetura"  > <img id="img_arquitetura" style=" z-index:1; " src="/imagens/'. $arquitetura[0]->nome_imagem .'" /> </div> <br/><br/> ';
//    }  width: 100%; height: 100%;
    ?>
    
    <!--< componentes %>-->
    <!--<a href="{{ route('arquitetura.edit',['id'=>$arquitetura[0]->id]) }}"> Editar </a>-->

@else
    
@endif
    
        
<script>
    var app = angular.module('app', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    
	 
    app.controller('appController', function ($scope, $http, $sce) {
        $scope.componentes = <?= $componentes ?>;
        
        $scope.desenha_elementos = function (componentes){
            var _html = "";
            var metade_div_quad = 25;
            var i; 
            var item;
            for(i = 0; i < componentes.length; i++){
                item = componentes[i];
                _html +=  "<div class='text_box' style='position: absolute;";
                _html += "left: "+(item.pos_x-metade_div_quad)+"px; ";
                _html += "top: " +(item.pos_y-metade_div_quad)+"px; z-index:10;' > ";
//              CONTEUDO
                
                
//              _html += '<button onclick="set_informacoes('+i+')"><i class="menu-icon fa fa-bell">?</i></button>';
               if(item.nome_imagem != ""){
                    _html += '<a href="http://localhost:8000/arquitetura/componente/'+item.id+'" >';
                    _html += '<i class="btn-rounded btn-primary btn-xs fa fa-external-link"> </i></a>';
                }
//                _html += "CONTEUDO "
                _html += "</div>";
            }
            
//          Inseri os elementos na tela
            $scope.snippet = $sce.trustAsHtml(_html);
        };
        
        $scope.desenha_elementos($scope.componentes);
    });
</script>



@endsection
