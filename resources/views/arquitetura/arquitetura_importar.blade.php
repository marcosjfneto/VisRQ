@extends('layouts.app')


@section('conteudo')

<!--
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.1/angular.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  -->

    <form action="{{ route('arquitetura.importar_post') }}" method="POST" class="panel form-horizontal" enctype="multipart/form-data">
        <div class="panel-heading">
            <span class="panel-title">Importar logs</span>
        </div>
        <div class="panel-body">
<!--            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr" >
                        <label class="control-label">Título</label>
                        <input type="text" name="titulo_logs" class="form-control" required>
                    </div>
                </div>
            </div>-->
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr" >
                        <label class="control-label">Arquivo</label>
                        <input type="file" name="logs" class="form-control"  required>
                    </div>
                </div>
            </div>
            
            <input type="hidden" name="id" value="0">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr  text-right">
                        <button  class="btn btn-primary"> Importar</button>
                    </div>
                </div>
            </div>
       </div>     
    </form>

@endsection
