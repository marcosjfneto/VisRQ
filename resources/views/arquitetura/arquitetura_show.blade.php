@extends('layouts.app')

@section('conteudo')
  

@if(count($arquitetura)==1)

<div id="div_pai" > 
        
  
    <?php
//    if( file_exists ( base_path() . '/public/imagens/'. $arquitetura[0]->nome_imagem ) ){
        echo '<b>'. $arquitetura[0]->titulo_grupo .'</b><br/><br/> ';
        echo ' <div id="div_img_arquitetura"  > <img id="img_arquitetura" style=" z-index:1; " src="/imagens/'. $arquitetura[0]->nome_imagem .'" /> </div> <br/><br/> ';
//    }  width: 100%; height: 100%;
    ?>
    
    <div class="row">
        <div class="col-sm-12">
            <!--text-right-->
            <div class="form-group no-margin-hr ">
                @if($arquitetura[0]->id_componente_pai > 0)
                    <a href="{{ route('arquitetura.show',['id'=>$arquitetura[0]->id_componente_pai])}}" class="btn btn-primary" > Pai </a>
                @endif
                @foreach(json_decode($diagramas_irmaos) as $diagrama)
                    <a href="{{ route('arquitetura.show',['id'=>$diagrama->id])}}" class="btn btn-success" >{{$diagrama->titulo_grupo}} </a>
                @endforeach
            </div>
        </div>
    </div>

@else
@endif
    
<!--como esta integrado com php, deve ficar na página-->        
<script>
    var app = angular.module('app', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    
    app.controller('appController', function ($scope, $http, $sce) {
        $scope.componentes = <?= $componentes ?>;
        
        $scope.compara_eficiencia = function (){
            $scope.componentes.sort(function(a, b){return a.avg_cpuTime - b.avg_cpuTime;});//pos_x
            var indicador = 0;
            
            for(i = 0; i < $scope.componentes.length; i++){
                indicador++;
                $scope.componentes[i].ind_eficiencia = indicador;
            };
        };
        $scope.compara_eficiencia();
        
        
        $scope.compara_confiabilidade = function (){
            $scope.componentes.sort(function(a, b){return a.qtd_exception - b.qtd_exception;});//pos_x
            var indicador = 0;
            
            for(i = 0; i < $scope.componentes.length; i++){
                indicador++;
                $scope.componentes[i].ind_confiabilidade = indicador;
            };
        };
        $scope.compara_confiabilidade();
        
        $scope.seta_cor = function(p1, p2, escala, valor){
            if (valor < p1*escala){         //abaixo do limite
                return "btn-success";
            } else if (valor > p2*escala) { //acima do limite
                return "btn-danger";
            } else {                 //dentro do limite
                return "btn-warning";
            }    
        };
        
        $scope.seta_cor = function(p1, p2, escala, valor){
            if (valor < p1*escala){         //abaixo do limite
                return "btn-success";
            } else if (valor > p2*escala) { //acima do limite
                return "btn-danger";
            } else {                 //dentro do limite
                return "btn-warning";
            }    
        };
        
        $scope.seta_cor_indicador = function(p1, p2, escala, valor){
            if (valor < p1*escala){         //abaixo do limite
                return "label-success";
            } else if (valor > p2*escala) { //acima do limite
                return "label-danger";
            } else {                 //dentro do limite
                return "label-warning";
            }    
        };
        
        $scope.desenha_elementos = function (componentes){
            var _html = "";
            var metade_div_quad = 20;
            var i; 
            var item;
            var valor = 5;
            
            for(i = 0; i < componentes.length; i++){
                item = componentes[i];
                
                _html +=  "<div class='text_box2' style='position: absolute;";
                _html += "left: "+(item.pos_x-metade_div_quad)+"px; ";
                _html += "top: " +(item.pos_y-metade_div_quad)+"px; z-index:10;' > ";
//              CONTEUDO


                if(item.nome_imagem != ""){
//                    _html += '<a href="http://localhost:8000/arquitetura/componente/'+item.id+'" >';
//                    _html += '<i class="btn-rounded btn-primary btn-xs fa fa-external-link"> </i></a>';
                    _html += '<a class="btn btn-xs btn-labeled btn-primary a_link" href="http://localhost:8000/arquitetura/componente/'+item.id+'" >';
                    _html += '<span class="btn-label icon fa fa-external-link a_link"> </span>';
                    _html += '</a>';
                } else{
                    _html += '<a class="btn btn-xs btn-labeled btn-default a_link">';
                    _html += '<span class="btn-label a_link"> </span>';
                    _html += '</a>';
                }
//                _html += '<a class="btn btn-xs btn-xs">';
//                _html += '<span class="btn-label icon fa fa-user"> </span>';
//                _html += '</a>';


//                TODO COLOCAR O ID COMO O "NOME DO PACOTE_ATRIBUTO MONITORADO" PARA USAR O JAVASCRIPT E JQUERY PARA ALTERA A COR DOS ELEMENTOS
//               replicar na tela show
                _html += '<a><i class="btn-rounded ';
                _html += $scope.seta_cor(item.desempenho_p1, item.desempenho_p2, item.desempenho_escala, item.avg_cpuTime); //para teste, colocar valor no lugar do artributo
                _html += ' btn-xs fa fa-bolt"> </i></a>';
                
                _html += '<a><i class="btn-rounded ';
                _html += $scope.seta_cor(item.confiabilidade_p1, item.confiabilidade_p2, item.confiabilidade_escala, item.qtd_exception);
                _html +=' btn-xs fa fa-check"> </i></a>';
                
                //IMPRESSAO DOS NUMEROS
//                _html += '<span class="label label-danger">21</span>';//TESTE

                _html += '<span class="label '+$scope.seta_cor_indicador(item.desempenho_p1, item.desempenho_p2, item.desempenho_escala, item.avg_cpuTime)+'"'
                _html += "style=\'position: absolute;";
                _html += "left: "+(-7)+"px; "; // -24, 20 top para ficar do lado
                _html += "top: " +(43)+"px;'>";
                _html += $scope.componentes[i].ind_eficiencia + '</span>';//DO LADO NÃO FICA BOM POIS PODE SER 1 OU 2 DIGITOS
                
                _html += '<span class="label '+$scope.seta_cor_indicador(item.confiabilidade_p1, item.confiabilidade_p2, item.confiabilidade_escala, item.qtd_exception)+'"'
                _html += "style=\'position: absolute;";
                _html += "left: "+(27)+"px; "; // -41, 20 top para ficar do lado
                _html += "top: " +(43)+"px;'>";
                _html += $scope.componentes[i].ind_confiabilidade + '</span>';//TESTE
//                
//                _html += '<span class="badge badge-danger"'
//                _html += "style=\'position: absolute;";
//                _html += "left: "+(-30)+"px; ";
//                _html += "top: " +(25)+"px;'>";
//                _html += '12</span>';//nao fica tão bom
                
//                _html += '<span class="label">55</span>';
//                _html += '<span class="label badge-danger">5</span>';
//                
//                
//                
//                
//                
//                _html += '<a><i class="btn-rounded btn-danger btn-xs fa fa-bolt"> </i></a>';
//                _html += '<a><i class="btn-rounded btn-success btn-xs fa fa-check"> </i></a>';
//                
// OUTROS ATRIBUTOS
//                _html += '<a><i class="btn-rounded btn-warning btn-xs fa fa-user"> </i></a>';
//                _html += '<a><i class="btn-rounded btn-success btn-xs fa fa-th-large"> </i></a>';
//                _html += '<a><i class="btn-rounded btn-warning btn-xs fa fa-wrench"> </i></a>';
//                _html += '<a><i class="btn-rounded btn-danger btn-xs fa fa-cogs"> </i></a>';

                _html += "</div>";
            }
            
//          Inseri os elementos na tela
            $scope.snippet = $sce.trustAsHtml(_html);
        };
        
        $scope.desenha_elementos($scope.componentes);
    });
</script>





@endsection
