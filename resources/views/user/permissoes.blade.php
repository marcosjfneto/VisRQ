@extends('layouts.app')
@section('conteudo')
    <div class="container-fluid" ng-app="app">
        <div class="panel-heading">
            <span class="panel-title">Permissões - {{$user->name}}</span>
        </div>
        
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="{{ route('user.permissoes_store',['id'=>$user->id])}}">
            <input type="hidden" name="_token" value="{{ csrf_token()}}">
 
             <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr">
                      <label class="control-label">Secretaria</label>
                        <!--<label class="control-label">Secretaria</label>-->
                        <!--<input type="textt" name="tipo" class="form-control">-->

                        <!--<select name="secretaria" class="form-control form-group-margin" ng-model="secretaria" ng-change="set_departamento()" required>-->
                            @foreach ($secretarias as $secretaria)
                                <div class="checkbox" style="margin: 0;">
                                <label>
                                    <input class="px" name="s_{{$secretaria->texto}}" type="checkbox" ng-checked="{{in_array($secretaria->id, $per_secretarias)}}" value="{{$secretaria->id}}">
                                    <span class="lbl">{{$secretaria->texto}}</span>
                                </label>
                                </div>
<!--                            <option value="{{ $secretaria->id }}">{{ $secretaria->texto }}</option>-->
                           @endforeach 
<!--                            <option ng-repeat="item in secretarias | orderBy:'texto'" ng-value = "item.id">
                                 <% item.texto %>
                            </option>-->
                        <!--</select>-->


                    </div>
                </div><!-- col-sm-6 -->
                <div class="col-sm-4">
                    <div class="form-group no-margin-hr">
                      
                        <label class="control-label">Tipos e Subtipos</label>
                         @foreach ($tipos as $tipo)
                            </br> <label class="control-label">{{$tipo->texto}}</label>
                            @foreach ($subtipos as $subtipo)
                                @if ($tipo->id == $subtipo->tipo->id )
                                    <div class="checkbox" style="margin: 0;">
                                           <label>
                                               <input class="px" name="{{$subtipo->texto}}" type="checkbox" ng-checked="{{in_array($subtipo->id, $per_subtipos)}}" value="{{$subtipo->id}}">
                                           <span class="lbl">{{$subtipo->texto}}</span>
                                           </label>
                                    </div>
                                @endif
                            @endforeach    
                         @endforeach    
                       
                        <!--<input type="text" name="subtipo" class="form-control">-->
<!--                       TODO cadastrar secretaria e deparatamento  com angular utilizando a ultima opção para mostrar o form-->
                        <!--<select name="departamento" class="form-control form-group-margin"  required>-->
                               <!--<option>1</option>-->
<!--                               <option ng-repeat="item in departamentos | orderBy:'texto'" ng-value = "item.id">
                                 <% item.texto %>
                               </option>-->
                        <!--</select>-->
                        
                </div><!-- col-sm-6 -->
            </div><!-- row -->
            </div>    
            
            <div class="panel-footer text-right">
                <button class="btn btn-primary">Enviar</button>
            </div>
        </form> 
        </div>
     </div>
  <script type="text/javaScript">

    var app = angular.module('app', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    </script>
@endsection
