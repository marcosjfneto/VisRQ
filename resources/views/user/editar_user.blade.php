@extends('layouts.app')
@section('conteudo')
    <div class="container-fluid" ng-app="app">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar cadastro</div>
                    <div class="panel-body">

                                                                                
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('user.update_user', ['id'=>$user->id]) }}">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Nome:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ $user->name}}" required="">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-4 control-label">Senha:</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Confirmar Senha:</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation" required="">
                                </div>
                            </div>
                            
                           

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn">
                                        Salvar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <script type="text/javaScript">

    var app = angular.module('app', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    </script>
@endsection
