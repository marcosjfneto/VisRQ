@extends('layouts.app')

@section('conteudo')

    <!--<hr>-->    
    
    <!--NOVO PAINEL-->
    <div class="panel-body" ng-app="app" ng-controller="appController">
        <div class="row">
            <div class="col-sm-10">
                <div class="col-sm-6">
                    <a class="btn btn-sm btn-labeled btn-success" href="{{ route('user.create') }}">
                        <span class="btn-label icon fa fa-plus"></span>
                        Novo
                    </a>
                </div>
                <div class="col-sm-6">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('user.edit_por_cpf') }}"> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
<!--                        <div class="form-group">
                            <label class="col-md-5 control-label"></label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="cpf" placeholder="CPF" required>
                            </div>-->
                        
<!--                                  <div class="col-md-2">
                          <button type="submit" class="btn fa fa-search">
                                    Pesquisar
                                </button>
                                <button class="btn btn-rounded btn-labeled">
                                    <span class="icon fa fa-search"></span>
                                    Pesquisar
                                </button>
                                <button class="btn btn-rounded btn-labeled fa fa-search">
                                    Pesquisar                                    
                                </button>
                            </div>
                        </div>-->
                    </form>
                </div>
                <br></br>
                <!-- Light table -->
                <div class="table-light">
                    <div class="table-header">
                        <div class="table-caption">
                            Usuários
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th width="110"></th>
                                <th>Nome</th>
                                <th>email</th>
                                <th width="190">Tipo</th>
                                <!--<th>Texto</th>-->

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>                                                
                                    <a href="{{ route('user.edit',['id'=>$item->id])}}"> <i class="btn-primary btn-xs fa fa-edit"></i></a>
                                    @if($item->id != Auth::user()->id)
                                        <!--<a href="{{ route('user.permissoes',['id'=>$item->id])}}"> <i class="btn-warning btn-xs fa fa-rub"></i></a>-->
                                        <a href="{{ route('user.destroy',['id'=>$item->id])}}"> <i class="btn-danger btn-xs fa fa-trash-o"></i></a>
                                    @endif
                                </td>
                                <td> 
                                    {{$item->name}}
                                </td>
                                <td>{{$item->email}}</td>
                                <td><?php switch($item->tipo){
                                    case 'admin':
                                        echo 'Administrador';
                                        break;
                                    case 'user':
                                        echo 'Usuário';
                                        break;
                                    case 'tec':
                                        echo 'Técnico';
                                        break;
                                    }
                                ?>
                                 </td>
                                <!--<td>{{$item->texto}}</td>-->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination center"> {!! $users->render() !!} </div>
                </div>
                <!-- / Light table -->
            </div>
        </div>
    </div>


</div>

 <script type="text/javaScript">

    var app = angular.module('app', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });

        

    app.controller('appController', function ($scope, $http) {

        
       $scope.cpf = "";
        // $scope.get_secretarias();
         
    });
</script>

<!--<div class="container">
    @foreach ($users as $user)
        {{ $user->name }}
    @endforeach
</div>-->

<!--<div class="pagination">{!! $users->render() !!}</div>-->
      
    
@stop