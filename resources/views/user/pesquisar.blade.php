@extends('layouts.app')
@section('conteudo')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Cadastro de usuário</div>
                    <div class="panel-body">

                        <!--"/auth/register"-->
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('user.store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">cpf:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cpf" value="" required>
                                </div>
                            </div>

                         
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn">
                                        Pesquisar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
