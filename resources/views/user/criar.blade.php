@extends('layouts.app')
@section('conteudo')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Cadastrar usuário</div>
                    <div class="panel-body">

                        <!--"/auth/register"-->
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('user.store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Nome:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                                </div>
                            </div>
                            
<!--                            <div class="form-group">
                                <label class="col-md-4 control-label">CPF:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cpf" value="{{ old('cpf') }}" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Matrícula:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="matricula" value="{{ old('matricula') }}" required>
                                </div>
                            </div>-->

                            <div class="form-group">
                                <label class="col-md-4 control-label">Email:</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Senha:</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Confirmar Senha:</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="type" class="col-md-4 control-label">Tipo</label>
                                <div class="col-md-6">
                                    <select name="type">
                                        <option value="user">Usuário</option>
                                        <!--<option value="tec">Técnico</option>-->
                                        <option value="admin">Administrador</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn">
                                        Salvar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
