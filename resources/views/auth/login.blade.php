@extends('layouts.app_base')

@section('conteudo')
			<!-- Right side -->
                        <div class="signin-form">

                            <!-- Form -->
                            <form action="{{ url('/login') }}" method="POST" id="signin-form_id">
                                {{ csrf_field() }}
                                <div class="signin-text">
                                    <span>Entre na sua conta</span>
                                </div> <!-- / .signin-text -->

                                <div class="form-group w-icon">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input type="email" name="email" id="username_id" class="form-control input-lg" placeholder="Email">
                                        <span class="fa fa-user signin-form-icon" value="{{ old('email') }}"></span>
                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div> <!-- / Username -->

                                <div class="form-group w-icon">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input type="password" name="password" id="password_id" class="form-control input-lg" placeholder="Password">
                                        <span class="fa fa-lock signin-form-icon"></span>
                                        @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div> <!-- / Password -->

                                <div class="form-actions">
                                    <input type="submit" value="Login" class="signin-btn bg-primary">
                                    <!--<a href="{{ url('/password/reset') }}" class="forgot-password" id="forgot-password-link">Esqueceu a senha?</a>-->
                                </div> 
                                <!-- / .form-actions -->
                            </form>
                        </div>   
                </div>
                	<!-- / Container para o bloco not-a-member ficar em baixo-->
                        
<!--                        <div class="not-a-member">
                            Não tem conta? <a href="/register">Clique aqui!</a>
                        </div>-->
			<!-- / Form -->
@endsection
