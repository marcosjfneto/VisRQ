@extends('layouts.app')

@section('conteudo')
<div >

    <!-- 6. $HORIZONTAL_FORM ===========================================================================

                                Horizontal form
    -->
    <form action="{{route('relatorio.post_logs_normais')}}" method="POST" class="panel form-horizontal" enctype="multipart/form-data">

        <div class="panel-heading">
            <span class="panel-title">Pesquisar logs normais</span>
        </div>
        
        <div class="panel-body">
            <div class="col-sm-4">
                <label class="control-label">Pacote</label>
                <select name="pacote" class="form-control form-group-margin">
                    <option value="0">GERAL</option>
                    @foreach ($componentes as $componente)
                    <option value="{{ $componente->pacote_componente }}">{{ $componente->pacote_componente }}</option>
                    @endforeach 
                </select>
            </div> 

            <div class="col-sm-2">
                <div class="form-group no-margin-hr">
                    <label class="control-label">Data inicial</label>
                    <input type="date" name="data_inicial" class="form-control" placeholder="yyyy-mm-dd">
                </div>
            </div><!-- col-sm-3 -->

            <div class="col-sm-2">
                <div class="form-group no-margin-hr">
                    <label class="control-label">Data final</label>
                    <input type="date" name="data_final" class="form-control" placeholder="yyyy-mm-dd">
                </div>
            </div><!-- col-sm-3 -->
            <div class="col-sm-2">
                <label class="control-label">Ordenar por</label>
                <select name="order" class="form-control form-group-margin">
                    <option value="id">id</option>
                    <option value="datetime">datetime</option>
                    <option value="executable">executable</option>
                </select>
            </div> 
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token()}}">
        <div class="panel-footer text-right">
            <button class="btn btn-primary">Pesquisar</button>
        </div>
    </form>
 
<!--    <div class="panel-heading">
            <span class="panel-title"> <h4>$titulo </h4> </span>
    </div>-->
    @if(count($logs))
    <h4><b> Logs normais </b></h4>
    <div>
        <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>executable</th>
                                <th>cpuTime</th>
                                <th>wallTime</th>
                                <th>args</th>
                                <th>returnType</th>
                                <th>result</th>
                                <th>datetime</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($logs as $log)
                            <tr>
                                <td>{{$log->id}}</td>
                                <td>{{$log->executable}}</td>
                                
                                <td>{{$log->cpuTime}}</td>
                                <td>{{$log->wallTime}}</td>
                                <td>{{$log->args}}</td>
                                <td>{{$log->returnType}}</td>
                                <td>{{$log->result}}</td>
                                <td>{{$log->datetime}}</td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
    </div>
    @endif 
</div>

<?php //$qtds; ?>


@endsection
