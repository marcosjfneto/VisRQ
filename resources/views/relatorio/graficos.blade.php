@extends('layouts.app')

@section('conteudo')
<div >

    <!-- 6. $HORIZONTAL_FORM ===========================================================================   Horizontal form   -->
    <form action="{{route('relatorio.post')}}" method="POST" class="panel form-horizontal" enctype="multipart/form-data">

        <div class="panel-heading">
            <span class="panel-title">Gerar gráficos</span>
        </div>
        
        <div class="panel-body">
            <div class="col-sm-6">
                <label class="control-label">Pacote</label>
                <select name="pacote" class="form-control form-group-margin">
                    <option value="0">GERAL</option>
                    @foreach ($componentes as $componente)
                    <option value="{{ $componente->pacote_componente }}">{{ $componente->pacote_componente }}</option>
                    @endforeach 
                </select>
            </div> 

            <div class="col-sm-2">
                <div class="form-group no-margin-hr">
                    <label class="control-label">Data inicial</label>
                    <input type="date" name="data_inicial" class="form-control" placeholder="yyyy-mm-dd">
                </div>
            </div><!-- col-sm-3 -->

            <div class="col-sm-2">
                <div class="form-group no-margin-hr">
                    <label class="control-label">Data final</label>
                    <input type="date" name="data_final" class="form-control" placeholder="yyyy-mm-dd">
                </div>
            </div><!-- col-sm-3 -->
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token()}}">
        <div class="panel-footer text-right">
            <button class="btn btn-primary">Pesquisar</button>
        </div>
    </form>
 
<!--    <div class="panel-heading">
            <span class="panel-title"> <h4>$titulo </h4> </span>
    </div>-->
    
    <h4> Média dos tempos de execução</h4>
    <div>
        <!-- 5. $UPLOADS_CHART =============================================================================Uploads chart -->
        <!-- Javascript -->
        <script>
            init.push(function () {
                var uploads_data = <?php echo $medias_tempos; ?>;

                Morris.Line({
                    element: 'hero-graph',
                    data: uploads_data,
                    xkey: 'day',
                    ykeys: ['ct', 'wt'],
                    labels: ['cpuTime', 'wallTime'],
                    lineColors: ['#fff', '#3C0'],
                    lineWidth: 2,
                    pointSize: 3,
                    gridLineColor: 'rgba(255,255,255,.5)',
                    resize: true,
                    gridTextColor: '#cfcfcf',
                    xLabels: "day",
                    xLabelFormat: function (d) {
                        return ['Jan', 'FeV', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'][d.getMonth()] + ' ' + d.getDate();
                    },
                });

            });
        </script>
        <!-- / Javascript -->
        <div class="stat-panel">
            <div class="stat-row">

                <!-- Primary background, small padding, vertically centered text -->
                <div class="stat-cell col-sm-8 bg-primary padding-sm valign-middle">
                    <div id="hero-graph" class="graph" style="height: 230px;"></div>
                </div>
            </div>
        </div>
    </div>

    <h4> Número de exceções</h4>
    <div>
        <!-- 5. $UPLOADS_CHART =============================================================================Uploads chart -->
        <!-- Javascript -->
        <script>
            init.push(function () {
                var uploads_data = <?php echo $qtd_excecoes; ?>;

                Morris.Line({
                    element: 'hero-graph2',
                    data: uploads_data,
                    xkey: 'day',
                    ykeys: ['c'],
                    labels: ['Quantidade'],
                    lineColors: ['#fff'],
                    lineWidth: 2,
                    pointSize: 3,
                    gridLineColor: 'rgba(255,255,255,.5)',
                    resize: true,
                    gridTextColor: '#cfcfcf',
                    xLabels: "day",
                    xLabelFormat: function (d) {
                        return ['Jan', 'FeV', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'][d.getMonth()] + ' ' + d.getDate();
                    },
                });

            });
        </script>
        <!-- / Javascript -->
        <div class="stat-panel">
            <div class="stat-row">

                <!-- Primary background, small padding, vertically centered text -->
                <div class="stat-cell col-sm-8 bg-danger padding-sm valign-middle">
                    <div id="hero-graph2" class="graph" style="height: 230px;"></div>
                </div>
            </div>
        </div>

    </div>
    
</div>

<?php $qtds; ?>


@endsection
