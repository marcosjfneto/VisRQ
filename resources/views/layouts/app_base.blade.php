<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>VisRQ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <!-- Open Sans font from Google CDN -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

    <!-- Pixel Admin's stylesheets -->
        <link href="{{ asset('html/assets/stylesheets/bootstrap.min.css')   }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('html/assets/stylesheets/pixel-admin.min.css')   }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('html/assets/stylesheets/widgets.min.css')   }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('html/assets/stylesheets/pages.min.css')   }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('html/assets/stylesheets/rtl.min.css')   }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('html/assets/stylesheets/themes.min.css')   }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('style.css')   }}" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script src="assets/javascripts/ie.min.js"></script>
    <![endif]-->


<!-- $DEMO =========================================================================================

    Remove this section on production
-->
    <style>
        #signin-demo {
            position: fixed;
            right: 0;
            bottom: 0;
            z-index: 10000;
            background: rgba(0,0,0,.6);
            padding: 6px;
            border-radius: 3px;
        }
        #signin-demo img { cursor: pointer; height: 40px; }
        #signin-demo img:hover { opacity: .5; }
        #signin-demo div {
            color: #fff;
            font-size: 10px;
            font-weight: 600;
            padding-bottom: 6px;
        }
    </style>
<!-- / $DEMO -->

</head>


<!-- 1. $BODY ======================================================================================
    
    Body

    Classes:
    * 'theme-{THEME NAME}'
    * 'right-to-left'     - Sets text direction to right-to-left
-->
<body class="theme-default page-signin">

<script>
    var init = [];
    init.push(function () {
        var $div = $('<div id="signin-demo" class="hidden-xs"><div>PAGE BACKGROUND</div></div>'),
            bgs  = [ 'assets/demo/signin-bg-1.jpg', 'assets/demo/signin-bg-2.jpg', 'assets/demo/signin-bg-3.jpg',
                     'assets/demo/signin-bg-4.jpg', 'assets/demo/signin-bg-5.jpg', 'assets/demo/signin-bg-6.jpg',
                     'assets/demo/signin-bg-7.jpg', 'assets/demo/signin-bg-8.jpg', 'assets/demo/signin-bg-9.jpg' ];
        for (var i=0, l=bgs.length; i < l; i++) $div.append($('<img src="' + bgs[i] + '">'));
        $div.find('img').click(function () {
            var img = new Image();
            img.onload = function () {
                $('#page-signin-bg > img').attr('src', img.src);
                $(window).resize();
            }
            img.src = $(this).attr('src');
        });
        $('body').append($div);
    });
</script>
<!-- Demo script --> <script src="assets/demo/demo.js"></script> <!-- / Demo script -->

    <!-- Page background -->
    <div id="page-signin-bg">
        <!-- Background overlay -->
        <div class="overlay"></div>
        <!-- Replace this with your bg image -->
        <img src="assets/demo/signin-bg-1.jpg" alt="">
    </div>
    <!-- / Page background -->

    <!-- Container -->
    <div class="signin-container">

        <!-- Left side -->
        <div class="signin-info">
            <a href="#" class="logo">
                <img src="{{ asset('logo40_40.png') }}" alt="" style="margin-top: -5px;">&nbsp;
                VisRQ
            </a> <!-- / .logo -->
            <div class="slogan">
                
            </div> <!-- / .slogan -->
            
            <ul>
<!--                <li><i class="fa fa-sitemap signin-icon"></i> Flexible modular structure</li>
                <li><i class="fa fa-file-text-o signin-icon"></i> LESS &amp; SCSS source files</li>
                <li><i class="fa fa-outdent signin-icon"></i> RTL direction support</li>
                <li><i class="fa fa-heart signin-icon"></i> Crafted with love</li>-->
            </ul> <!-- / Info list -->
        </div>
        <!-- / Left side -->
                @yield("conteudo")
                
                

        </div>
    <!-- / Container -->
        <div class="not-a-member">
<!--            Not a member?
            <a href="pages-signup.html">Sign up now</a>-->

             <!--<h6 ><b>Prefeitura Municipal de Arapiraca ---> 
                <?php
//                $date=date_create("");
//                echo date_format($date,"Y");
                ?>                
            <!--</b>
                 </br></br>


                Desenvolvido por:
                <div class="img-logo">

                    <img id="img-gtinfo" src="gtinfoblog.png">
                    
                </div >
            </h6>
            -->
        </div>
        
<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
    <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
    <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- Pixel Admin's javascripts -->
<script src="assets/javascripts/bootstrap.min.js"></script>
<script src="assets/javascripts/pixel-admin.min.js"></script>

<script type="text/javascript">
    // Resize BG
    init.push(function () {
        var $ph  = $('#page-signin-bg'),
            $img = $ph.find('> img');

        $(window).on('resize', function () {
            $img.attr('style', '');
            if ($img.height() < $ph.height()) {
                $img.css({
                    height: '100%',
                    width: 'auto'
                });
            }
        });
    });

    // Show/Hide password reset form on click
    
    // Setup Sign In form validation
    
    // Setup Password Reset form validation
    
    window.PixelAdmin.start(init);
</script>

<!--<footer class="footer span1 center" id="footer"  >
            
            <h6 ><b>Prefeitura Municipal de Arapiraca - 
                <?php
//                $date=date_create("");
//                echo date_format($date,"Y");
                ?>                
            </b>

                <div align="right" class="img-logo">
                    Desenvolvido por:
                    <div >
                          <img id="img-gtinfo" src="/images/gtinfoblog.png">
                    </div>
                </div >
            </h6>
                
</footer>-->

</body>
</html>

