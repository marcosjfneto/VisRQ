@extends('layouts.app')

@section('conteudo')
<div >
    
    <!-- 6. $HORIZONTAL_FORM ===========================================================================

                                Horizontal form
    -->
    <form action="{{ route('pacote.store') }}" method="POST" class="panel form-horizontal">
        <div class="panel-heading">
            <span class="panel-title">Cadastrar Pacote</span>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr">
                        <label class="control-label">Nome</label>
                        <input type="nome" name="nome" class="form-control" required>
                    </div>
                </div><!-- col-sm-6 -->
            </div><!-- row -->

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr  text-right">
                        <button class="btn btn-primary">Salvar</button>
                    </div>
                </div><!-- col-sm-6 -->
            </div><!-- row -->

            
        </div>
    </form>

</div>
@endsection
