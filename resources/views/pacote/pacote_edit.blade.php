@extends('layouts.app')

@section('conteudo')
<div >
    
    <!-- 6. $HORIZONTAL_FORM ===========================================================================

                                Horizontal form
    -->
    <form action="{{ route('pacote.update',['id'=>$pacote->id]) }}" method="POST" class="panel form-horizontal">
        <div class="panel-heading">
            <span class="panel-title">Editar Tipo</span>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr">
                        <label class="control-label">Nome</label>
                        <input type="text" name="nome" value="{{$pacote->texto}}" class="form-control" required>
                    </div>
                </div><!-- col-sm-6 -->
            </div><!-- row -->

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$pacote->id}}">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group no-margin-hr  text-right">
                        <button class="btn btn-primary">Salvar</button>
                    </div>
                </div><!-- col-sm-6 -->
            </div><!-- row -->

            
        </div>
    </form>

</div>
@endsection
