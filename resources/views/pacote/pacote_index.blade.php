@extends('layouts.app')

@section('conteudo')
<div >
    
    
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                
                    <a class="btn btn-sm btn-labeled btn-success" href="{{ route('pacote.create') }}">
                        <span class="btn-label icon fa fa-plus"></span>
                        Novo
                    </a>
                
                <br></br>
                <!-- Light table -->
                <div class="table-light">
                    <div class="table-header">
                        <div class="table-caption">
                            Pacotes
                        </div>
                    </div>

                @if($pacotes->count())
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Ações</th>
                                <th>Texto</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pacotes as $pacote)
                            <tr>
                                <td>{{$pacote->id}}</td>
                                <td> 
                                    <a href="{{ route('pacote.edit',['id'=>$pacote->id])}}"><i class="menu-icon btn-primary btn-xs fa fa-edit"></i></a>
                                    <a href="{{ route('pacote.destroy',['id'=>$pacote->id])}}"><i class="menu-icon btn-danger btn-xs fa fa-trash-o"></i></a>
                                    
                                </td>
                                <td>{{$pacote->nome}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                @else
                    <h2>Nenhum pacote cadastrado.</h2>
                @endif  

                </div>
                <!-- / Light table -->
            </div>
        </div>
    </div>


</div>
@endsection
