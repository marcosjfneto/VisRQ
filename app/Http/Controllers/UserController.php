<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use app\Http\Requests;
use app\User;
use app\Entities\Secretaria;
use app\Entities\Subtipo;
use app\Entities\Tipo;
use DB;
use Auth;

class UserController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $this->authorize('is_admin', $user);
        
        $users = User::where('ativo', 1)->paginate(15);
        return view('user.index', ['users' => $users]);
    }

    public function permissoes($id)
    {
        $user_logado = Auth::user();
        $this->authorize('is_admin', $user_logado); 
        
        $user = User::find($id);
        $secretarias = Secretaria::all();
        $subtipos = Subtipo::all();
        $tipos = Tipo::all();
       
//        $permissoes =  DB::select('select * from permissoes where id_user = ?', [$id]);
//        print_r($permissoes);
//        print_r($permissoes[0]['secretarias']);
//        print_r($permissoes[0]['subtipos']);
        
        $per_secretarias = [];
        $per_subtipos = [];
       
       try {
          $linha = DB::select('select * from permissoes where id_user = :id', ['id' => $id])[0];
          $array_linha = (array) $linha;
          $per_secretarias = json_decode( $array_linha ['secretarias'] );
          $per_subtipos = json_decode( $array_linha ['subtipos'] );
       } catch (\Exception $e){
//           \Session::flash('alert','Permissões não estão cadastradas!');
//           return view('chamado.chamado_listar', ['chamados' => $chamados]);
       }
        
        return view('user.permissoes', ['user' => $user, 'secretarias'=>$secretarias, 'tipos'=>$tipos, 'subtipos'=>$subtipos, 
                                        'per_secretarias'=>$per_secretarias, 'per_subtipos'=>$per_subtipos]);
    }
    
    public function permissoes_store(Request $request, $id)
    {
      $user = Auth::user();
      $this->authorize('is_admin', $user); 
        
            
      $sec = [];
      $subtipos = [];
      foreach ( $request->all() as $key => $value) { 
            switch ($key) {
            case '_token':
                break;
            default:
                if(substr_count($key, 's_')==1){
                    array_push($sec, $request[$key]);
                } else {
                    array_push($subtipos, $request[$key]);
                };
                break;
            }
        }

       $linha = DB::select('select * from permissoes where id_user = ?', [$id]);
       if ($linha){
           DB::update('update permissoes set secretarias = ?, subtipos = ? where id_user = ?', [json_encode($sec), json_encode($subtipos), $id]);
       } else {
           DB::insert('insert into permissoes (id_user, secretarias, subtipos) values (?, ?, ?)', [$id, json_encode($sec), json_encode($subtipos)]);
       }
       
       \Session::flash('success','Permissões alteradas!');
        return redirect(route('user.index'));
    }
    
    
    public function create()
    {
        $user = Auth::user();
        $this->authorize('is_admin', $user);
        return view('user.criar');
    }

    public function store(Request $request)//Request
    {
        $user = Auth::user();
        $this->authorize('is_admin', $user);
        
        if (User::where('email', $request['email'])->count()){
            return redirect()->back()
                    ->with('alert', 'O email '.$request['email'].' já está cadastrado no sistema!')->withInput();
        };
        
        //verificações
        $funcionario = DB::select('select * from funcionarios where cpf = ? and matricula = ?', [$request['cpf'], $request['matricula']]);
        if (count($funcionario)==0){
               \Session::flash('alert','O CPF '.$request['cpf'].' ou a matrícula '.$request['matricula'].' não está cadastrado!');
               return redirect('/login');
        } elseif ($funcionario[0]->cadastrado==TRUE){
            \Session::flash('alert','O usuário já está cadastrado!');
            return redirect('/login');
        }
        
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'cpf' => $request['cpf'],
            'matricula' => $request['matricula'],
            'password' => bcrypt( $request['password'] ),
            'tipo'=>$request['type'],
        ]);
        
        \Session::flash('success','Usuário cadastrado!');
        return redirect()->route('user.index')->with('status', 
                    'Usuário criado com sucesso!');
    }

    public function edit($id)
    {
        $user_logado = Auth::user();
        $this->authorize('is_admin', $user_logado);
        
        $user = User::find($id);
        return view('user.editar', ['user' => $user]);
    }
    
    public function update(Request $request, $id)
    {
        $user_logado = Auth::user();
        $this->authorize('is_admin', $user_logado);
        
        User::where('id', $id)
                ->update([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt( $request['password'] ),
                'tipo'=>$request['type'],
                ]);
        
        \Session::flash('success','Usuário alterado!');
        return redirect()->route('user.index');
    }
    
      public function edit_por_cpf(Request $request)
    {
          print_r('edit_por_cpf');
          print_r($request['cpf']);
        $user_logado = Auth::user();
        $this->authorize('is_admin', $user_logado);
        
        $user = User::where('cpf', $request['cpf'])->get();
        if (count($user)==0){
            \Session::flash('alert','O usuário com CPF '. $request['cpf']. ' não foi encontrado!');
            return redirect()->back();
        } else {
            return view('user.editar', ['user' => $user[0]]);
        }
    }
    
     public function edit_user($id)
    {
        $user = User::find($id);
        $user_logado = Auth::user();
        $this->authorize('mesmo_user', $user_logado, $user);
        
        return view('user.editar_user', ['user' => $user_logado]);
    }
    
    public function update_user(Request $request, $id)//Request
    {
        $user = User::find($id);
        $user_logado = Auth::user();
        $this->authorize('mesmo_user', $user_logado, $user);
        
        User::where('id', $id)
                ->update([
                'name' => $request['name'],
                'password' => bcrypt( $request['password'] ),
                ]);
        
        \Session::flash('success','Usuário alterado!');
        return redirect('/home');
    }

    public function destroy($id)
    {
        $user_logado = Auth::user();
        $this->authorize('is_admin', $user_logado);
        
        \Session::flash('success','Usuário excluído!');
        DB::update('update users set ativo = ? where id = ?', [0, $id]);
        return redirect()->route('user.index');
    }
}
