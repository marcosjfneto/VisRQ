<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use app\Entities\Arquitetura;
use Auth;
use DB;

class ArquiteturaController extends Controller
{
//string com endereço localhost: arquitetura_editar, arquitetura_show
    
    public function importar_get() {//por enquanto, trabalha apenas com 1 projeto por vez
        $user = Auth::user();
//        $this->authorize('is_admin', $user);
             
//        TESTE
//        $arquivo = file_get_contents('logs/abnormal.log');//.'.txt'
//        $arquivo = '['.str_replace("}\n{","},\n{",$arquivo).']';
//        $logs_json = json_decode($arquivo, true);
////        var_dump($logs_json);
//        
//        if (array_key_exists("exception",$logs_json[0])){
//            foreach ($logs_json as $log) {
//                $id = DB::table('anormal_logs')->insertGetId([
//                    'datetime' => $log['datetime'],
//                    'executable' => $log['executable'],
//                    'args' => json_encode($log['args']),
//                    'cpuTime' => $log['cpuTime'],
//                    'wallTime' => $log['wallTime'],
//                    'returnType' => $log['returnType'],
//                    'exception_name'=> $log['exception']['name'],
//                    'exception_file'=> $log['exception']['file'],
//                    'exception_line'=> $log['exception']['line'],
//                    'exception_message'=> $log['exception']['message'],
//                ]);
//            }
//        } else {
//            foreach ($logs_json as $log) {
//                $id = DB::table('normal_logs')->insertGetId([
//                    'datetime' => $log['datetime'],
//                    'executable' => $log['executable'],
//                    'args' => json_encode($log['args']),
//                    'cpuTime' => $log['cpuTime'],
//                    'wallTime' => $log['wallTime'],
//                    'returnType' => $log['returnType'],
//                    'result' => $log['result'],
//                ]);
//            }
//        }
        
//        $arquitetura = DB::select('select * from componentes where id_componente_pai = 0');//? AS 'pacote',
//        $elementos = DB::select("SELECT ? AS 'pacote', AVG(`cpuTime`) AS 'avg_cpuTime', AVG(`wallTime`) AS 'avg_wallTime' "
//                . "FROM `normal_logs` "
//                . "WHERE `executable` LIKE '?%'", ['br.com2', 'br.com2']);
        
        
//        $elementos = array();
//        $componentes = DB::select('select * from componentes where id_componente_pai = 0');//.$id);
//        foreach ($componentes as $componente){
//            $log = DB::select("SELECT  AVG(`cpuTime`) AS 'avg_cpuTime', AVG(`wallTime`) AS 'avg_wallTime' " 
//                                ."FROM `normal_logs` " 
//                                ."WHERE `executable` LIKE '".$componente->pacote_componente."%'");
//            
//            $ar_merge = array_merge( (array)$componente, (array)$log[0] );
//            array_push($elementos, $ar_merge);
//        }
//        dd($elementos);
        
        return view('arquitetura.arquitetura_importar');//, ['id'=>$id]
    }
    
    public function importar_post(Request $request){
        $user = Auth::user();
//        $this->authorize('is_admin', $user);
//        TODO TRATAR O ARQUIVO
        
//        $titulo = $request['titulo_logs'];
//        $titulo = 'log_1';

        if ($request->hasFile('logs')) {
//            $request->file('logs')->move(base_path() . 'logs/', $titulo.'.txt');//$nome_imagem
//            $arquivo = file_get_contents('logs/'.$titulo.'.txt');
            
//          Por enquanto, armazena temporariamente os logs para poder ser lido
//          Se não está escrevento o arquivo logs.txt, basta executar chmod 777 logs
            $request->file('logs')->move(base_path() . '/public/logs/', 'logs_temp.txt');//$nome_imagem
//            $arquivo = file_get_contents('logs/'.'abnormal.log');
//            $arquivo = file_get_contents('logs/'.'abnormal.txt');
            
//            $arquivo = file_get_contents('logs/'.'abnormal.log');//passar logs.txt para leitura
            
            $arquivo = file_get_contents('logs/'.'logs_temp.txt');
            $arquivo = '[' . str_replace("}\n{", "},\n{", $arquivo) . ']';
//            dd($arquivo);
            $logs_json = json_decode($arquivo, true);
//            dd($logs_json);
            
            if (array_key_exists("exception",$logs_json[0])){
                foreach ($logs_json as $log) {
                    $id = DB::table('anormal_logs')->insertGetId([
                        'datetime' => $log['datetime'],
                        'executable' => $log['executable'],
                        'args' => json_encode($log['args']),
                        'cpuTime' => $log['cpuTime'],
                        'wallTime' => $log['wallTime'],
                        'returnType' => $log['returnType'],
                        'exception_name'=> $log['exception']['name'],
                        'exception_file'=> $log['exception']['file'],
                        'exception_line'=> $log['exception']['line'],
                        'exception_message'=> $log['exception']['message'],
                    ]);
                }
                \Session::flash('success','Logs importados com sucesso!');
            } else {
    //            normal
                foreach ($logs_json as $log) {
                    $id = DB::table('normal_logs')->insertGetId([
                        'datetime' => $log['datetime'],
                        'executable' => $log['executable'],
                        'args' => json_encode($log['args']),
                        'cpuTime' => $log['cpuTime'],
                        'wallTime' => $log['wallTime'],
                        'returnType' => $log['returnType'],
                        'result' => $log['result'],
                    ]);
                }
                \Session::flash('success','Logs importados com sucesso!');
            }
        }
        return redirect()->back();
    }
    
    protected function importar_outro_diagrama_post(Request $request){
        $user = Auth::user();
//        $this->authorize('is_admin', $user);

        $file =  $request->file('diagrama');
        $nome_imagem = $file->getClientOriginalName();
        
//        $id = DB::table('outros_diagramas')->insertGetId([
//                    'id_componente_pai' => 0,
//                    'id_componente_irmao' => 0,
//                    'nome_imagem' => $nome_imagem,
////                    'pacote_componente' => '',//$request['pacote_componente'],
//                ]);
        
//        VER SE USO ISSO OU NÃO
        $id = DB::table('componentes')->insertGetId([
                    'titulo_grupo' => $request['titulo_diagrama'],
                    'id_componente_pai' => $request['id_componente_pai'],
                    'id_componente_irmao' => $request['id_componente_irmao'],
                    'nome_imagem' => $nome_imagem, 
                    'e_diagrama' => 1,
                ]);
        
        if ($request->hasFile('diagrama')) {
            $file->move(base_path() . '/public/imagens/', $nome_imagem);
        }
        
//        \Session::flash('success','Arquitetura principal salva!');
        return redirect()->route('arquitetura.index');
    }
    
    public function index() { //METODO PARA EXIBIR MAIS COMPLETO
        $user = Auth::user();
//        $this->authorize('is_admin', $user);
        
//         $arquiteturas = Arquitetura::paginate(20);//where('ativo', 1)->
//         return view('arquitetura.arquitetura_index', ['arquiteturas' => $arquiteturas]);
        
        $arquitetura = DB::select('select * from componentes where id_componente_pai = 0');
        
        
        if(count($arquitetura)>0){//mudar de ==1 para > 
            $diagramas_irmaos = DB::select('select * from componentes where id_componente_irmao = ?',[$arquitetura[0]->id_componente_irmao]);//
//            var_dump($diagramas_irmaos);
            $componentes = $this->contabiliza_metricas($arquitetura[0]->id);
//            dd($arquitetura);
            return view('arquitetura.arquitetura_index', ['componentes' => json_encode($componentes), 
                                                            'arquitetura'=>$arquitetura, 'diagramas_irmaos'=>json_encode($diagramas_irmaos)]);
        }
        return view('arquitetura.arquitetura_index', ['componentes' => json_encode([]), 'arquitetura'=>[], 'diagramas_irmaos'=>[]]);
    }
    
    public function show($id)
    {
        $user = Auth::user();
//        $this->authorize('is_admin', $user);
        
        $arquitetura = DB::select('select * from componentes where id = '.$id);
        $diagramas_irmaos = DB::select('select * from componentes where id_componente_irmao = ?',[$arquitetura[0]->id_componente_irmao]);//
//        dd($diagramas_irmaos);
        $componentes = $this->contabiliza_metricas($id);
        return view('arquitetura.arquitetura_show', ['componentes' => json_encode($componentes), 'arquitetura'=>$arquitetura, 
                                                        'diagramas_irmaos'=>json_encode($diagramas_irmaos)]);
    }
    
    
    
    protected function contabiliza_metricas($id_componente_pai){
        $componentes_vis_db = DB::select('select * from componentes where id_componente_pai = '.$id_componente_pai.' AND e_diagrama = 0');//
        $componentes = array();
        
        foreach ($componentes_vis_db as $componente){
           $normal_log = DB::select("SELECT  AVG(`cpuTime`) AS 'avg_cpuTime', AVG(`wallTime`) AS 'avg_wallTime' "
                                . "FROM `normal_logs` "
                                . "WHERE `executable` LIKE '" . $componente->pacote_componente . ".%'"
                                );
                
           $anormal_log = DB::select("SELECT  COUNT(*) AS 'qtd_exception' "
                                . "FROM `anormal_logs` "
                                . "WHERE `executable` LIKE '" . $componente->pacote_componente . ".%'");
                
           $ar_merge = array_merge((array) $componente, (array) $normal_log[0], (array) $anormal_log[0]);//junta os arrays
           array_push($componentes, $ar_merge); //adiciona o novo elemento
        }
        return $componentes;
    }

    public function create()
    {
        $user = Auth::user();
//        $this->authorize('is_admin', $user);
        return view('arquitetura.arquitetura_create');
    }
    
    public function store_arquitetura(Request $request)
    {
        $user = Auth::user();
//        $this->authorize('is_admin', $user);
        

        $file =  $request->file('arquitetura_principal');
        $nome_imagem = $file->getClientOriginalName();
        
//        ver a necessidade de informar o pacote base para diferenciar de outros projetos
        $id = DB::table('componentes')->insertGetId([
                    'titulo_grupo' => $request['titulo_grupo'],
                    'pacote_componente' => '',//$request['pacote_componente'],
                    'id_componente_pai' => 0,
                    'nome_imagem' => $nome_imagem,
            
//                    'desempenho_escala' => $request['desempenho_escala'],
//                    'desempenho_p1' => $request['dese_p1'],
//                    'desempenho_p2' => $request['dese_p2'],
//                    'confiabilidade_escala' => $request['confiabilidade_escala'],
//                    'confiabilidade_p1' => $request['conf_p1'],
//                    'confiabilidade_p2' => $request['conf_p2'],
                ]);
        
        if ($request->hasFile('arquitetura_principal')) {
            $request->file('arquitetura_principal')->move(base_path() . '/public/imagens/', $nome_imagem);
        }
        
         DB::table('componentes')->where('id', $id)->update([
                    'id_componente_irmao' => $id,
                ]);
        
//        \Session::flash('success','Arquitetura principal salva!');
        return redirect()->route('arquitetura.index');
    }
    
    public function store_componente(Request $request)
    {
        $user = Auth::user();
        //        $this->authorize('is_admin', $user);

        $titulo_componente = $request['titulo_grupo'];
        $pacote_componente = $request['pacote_componente'];
        $id_pai = $request['id_componente_pai'];
        $pos_x = $request['pos_x'];
        $pos_y = $request['pos_y'];
        $nome_imagem = $request->hasFile('diagrama_componentes') ? $titulo_componente : "";

        $id = DB::table('componentes')->insertGetId([
                    'titulo_grupo' => $titulo_componente, 
                    'pacote_componente' => $pacote_componente,
                    'id_componente_pai' => $id_pai,
                    'pos_x' => $pos_x, 
                    'pos_y' => $pos_y,
                    'nome_imagem' => $nome_imagem, 
            
                    'desempenho_escala' => $request['desempenho_escala'],
                    'desempenho_p1' => $request['dese_p1'],
                    'desempenho_p2' => $request['dese_p2'],
                    'confiabilidade_escala' => $request['confiabilidade_escala'],
                    'confiabilidade_p1' => $request['conf_p1'],
                    'confiabilidade_p2' => $request['conf_p2'],
                ]);
        
//                NOVO TRECHO PARA FAZER O RELACIONAMENTO
                DB::table('componentes')->where('id', $id)->update([
                    'id_componente_irmao' => $id,
                ]);
        
        if ($request->hasFile('diagrama_componentes')) {
            $request->file('diagrama_componentes')->move(base_path() . '/public/imagens/', $nome_imagem );
        }       
        
        return redirect()->route('arquitetura.edit', ['id'=>$id_pai]);
//        \Session::flash('success','Componente salvo com sucesso!');
    }
    
    public function edit($id)
    {
        $user = Auth::user();
//        $this->authorize('is_admin', $user);
        
        $arquitetura = DB::select('select * from componentes where id = '.$id);
        
        $componentes = DB::select('select * from componentes where id_componente_pai = '.$id);
        return view('arquitetura.arquitetura_edit', ['componentes' => json_encode($componentes), 'arquitetura'=>$arquitetura]);
    }


    public function update(Request $request, $id)
    {
        $user = Auth::user();
//        $this->authorize('is_admin', $user);
        
        $titulo_componente = $request['titulo_grupo'];
        $pacote_componente = $request['pacote_componente'];
        $id_pai = $request['id_componente_pai'];
        $pos_x = $request['pos_x'];
        $pos_y = $request['pos_y'];
        $nome_imagem = $request->hasFile('diagrama_componentes') ? $titulo_componente : "";

        $id = DB::table('componentes')->where('id', $id)->update([
                    'titulo_grupo' => $titulo_componente, 
                    'pacote_componente' => $pacote_componente,
                    'id_componente_pai' => $id_pai,
                    'pos_x' => $pos_x, 
                    'pos_y' => $pos_y,
                    'nome_imagem' => $nome_imagem, 
            
                    'desempenho_escala' => $request['desempenho_escala'],
                    'desempenho_p1' => $request['dese_p1'],
                    'desempenho_p2' => $request['dese_p2'],
                    'confiabilidade_escala' => $request['confiabilidade_escala'],
                    'confiabilidade_p1' => $request['conf_p1'],
                    'confiabilidade_p2' => $request['conf_p2'],
                ]);
        if ($request->hasFile('diagrama_componentes')) {
            $request->file('diagrama_componentes')->move(base_path() . '/public/imagens/', $nome_imagem );
        }       
        
        return redirect()->route('arquitetura.edit', ['id'=>$id_pai]);
        
//        \Session::flash('success','Arquitetura alterada!');
//        return redirect()->route('arquitetura.index');
    }
    
  
    public function destroy($id)
    {   
        $user = Auth::user();
//        $this->authorize('is_admin', $user);
        
        DB::delete('delete from componentes where id = ?', [$id]);

//        \Session::flash('success','Arquitetura excluída!');
        return redirect()->back();//->route('arquitetura.index');
    }
}
