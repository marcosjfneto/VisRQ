<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;

use app\Http\Requests;
use Auth;
use DB;

//vale a pena fazer uma ferramenta de pesquisa envolvendo os vários parametros dos logs?
//por pacote, entre dadas, por tempo gasto, tipo de exceções, 
//deixar os campos de pesquisa flexiveis

class RelatorioController extends Controller
{
    //não usado
    public function index(){//Request $request
          $user = Auth::user();
//        $this->authorize('is_admin', $user);

//          fazer pesquisa entre datas
//          mudar a cor do grafico utilizando o id
        $componentes = DB::select(
            ' SELECT pacote_componente FROM `componentes` WHERE pacote_componente != ""'   // AS pacote
                );
          
        $medias_res = DB::select(
            ' SELECT DATE_FORMAT(`datetime`, "%Y-%m-%d") AS `day`, AVG(cpuTime) AS ct, AVG(wallTime) AS wt FROM `normal_logs` '   //, count(*) AS "c"
//            . ' WHERE DATE_FORMAT(`datetime`, "%Y-%m-%d" ) >= "2016-09-05" '
//            . ' AND DATE_FORMAT(`datetime`, "%Y-%m-%d" ) <= "2016-13-05" '
            . ' GROUP BY `day`'//,
                );//[ $data_inicial, $data_final]
        

        $medias_tempos = array();
        foreach ($medias_res as $qtd) {
            array_push($medias_tempos, (array) $qtd);
        }
        
        $excecoes_res = DB::select(
            ' SELECT DATE_FORMAT(`datetime`, "%Y-%m-%d") AS `day`, count(*) AS c FROM `anormal_logs` '   //, count(*) AS "c"
//            . ' WHERE DATE_FORMAT(`datetime`, "%Y-%m-%d" ) >= "2016-09-05" '
//            . ' AND DATE_FORMAT(`datetime`, "%Y-%m-%d" ) <= "2016-13-05" '
            . ' GROUP BY `day`'//,
                );//[ $data_inicial, $data_final]
        
        $qtd_excecoes = array();
        foreach ($excecoes_res as $qtd) {
            array_push($qtd_excecoes, (array) $qtd);
        }
        return view('relatorio.graficos', [
            'componentes'=>$componentes, 'medias_tempos'=> json_encode($medias_tempos), 
            'qtd_excecoes'=> json_encode($excecoes_res) ]);// json_encode($qtds)]);
    }
    
    public function relatorio_get(){
        $user = Auth::user();
//        $this->authorize('is_admin', $user);
         
        $data_inicial = "2010-01-01";
        $data_final =   date('Y-m-d');

//          fazer pesquisa entre datas
//          mudar a cor do grafico utilizando o id
        $componentes = DB::select(
            ' SELECT pacote_componente FROM `componentes` WHERE pacote_componente != ""'   // AS pacote
                );
//          
        $medias_res = DB::select(
            ' SELECT DATE_FORMAT(`datetime`, "%Y-%m-%d") AS `day`, AVG(cpuTime) AS ct, AVG(wallTime) AS wt FROM `normal_logs` '   //, count(*) AS "c"
            . ' WHERE DATE_FORMAT(`datetime`, "%Y-%m-%d" ) >= ? '
            . ' AND DATE_FORMAT(`datetime`, "%Y-%m-%d" ) <= ? '
            . ' GROUP BY `day`',
              [ $data_inicial, $data_final]);
        

        $medias_tempos = array();
        foreach ($medias_res as $qtd) {
            array_push($medias_tempos, (array) $qtd);
        }

        $excecoes_res = DB::select(
            ' SELECT DATE_FORMAT(`datetime`, "%Y-%m-%d") AS `day`, count(*) AS c FROM `anormal_logs` '   //, count(*) AS "c"
            . ' WHERE DATE_FORMAT(`datetime`, "%Y-%m-%d" ) >= ? '
            . ' AND DATE_FORMAT(`datetime`, "%Y-%m-%d" ) <= ? '
            . ' GROUP BY `day`',
                [ $data_inicial, $data_final]);//
        
        $qtd_excecoes = array();
        foreach ($excecoes_res as $qtd) {
            array_push($qtd_excecoes, (array) $qtd);
        }
        
        return view('relatorio.graficos', ['componentes'=>$componentes, 
                'medias_tempos'=> json_encode($medias_tempos), 
                'qtd_excecoes'=> json_encode($qtd_excecoes) ]);// json_encode($qtds)]);
    }
    
    public function relatorio_post(Request $request){
         $user = Auth::user();
//        $this->authorize('is_admin', $user);

         $data_inicial = $request->data_inicial != "" ? $request->data_inicial : "2010-01-01";
         $data_final = $request->data_final != "" ? $request->data_final : $date = date('Y-m-d');
         
//          fazer pesquisa entre datas
//          mudar a cor do grafico utilizando o id
        $componentes = DB::select(
            ' SELECT pacote_componente FROM `componentes` WHERE pacote_componente != ""'   // AS pacote
                );
          
        $medias_res = DB::select(
            ' SELECT DATE_FORMAT(`datetime`, "%Y-%m-%d") AS `day`, AVG(cpuTime) AS ct, AVG(wallTime) AS wt FROM `normal_logs` '   //, count(*) AS "c"
            . ' WHERE DATE_FORMAT(`datetime`, "%Y-%m-%d" ) >= ? '
            . ' AND DATE_FORMAT(`datetime`, "%Y-%m-%d" ) <= ? '
            . ' GROUP BY `day`',
              [ $data_inicial, $data_final]);
        

        $medias_tempos = array();
        foreach ($medias_res as $qtd) {
            array_push($medias_tempos, (array) $qtd);
        }
        
        $excecoes_res = DB::select(
            ' SELECT DATE_FORMAT(`datetime`, "%Y-%m-%d") AS `day`, count(*) AS c FROM `anormal_logs` '   //, count(*) AS "c"
             . ' WHERE DATE_FORMAT(`datetime`, "%Y-%m-%d" ) >= ? '
            . ' AND DATE_FORMAT(`datetime`, "%Y-%m-%d" ) <= ? '
            . ' GROUP BY `day`',
                [ $data_inicial, $data_final]);
        
        $qtd_excecoes = array();
        foreach ($excecoes_res as $qtd) {
            array_push($qtd_excecoes, (array) $qtd);
        }
        return view('relatorio.graficos', ['componentes'=>$componentes, 
            'medias_tempos'=> json_encode($medias_tempos), 'qtd_excecoes'=> json_encode($excecoes_res) ]);// json_encode($qtds)]);
    }
    
    
    //    NORMAIS - MODIFICAR
    public function relatorio_get_logs_normais(){
        $user = Auth::user();
//        $this->authorize('is_admin', $user);

        $componentes = DB::select(
            ' SELECT pacote_componente FROM `componentes` WHERE pacote_componente != ""'   // AS pacote
                );
          
        return view('relatorio.logs_normais', ['componentes'=>$componentes, 'logs'=>[], 
                 ]);// json_encode($qtds)]);
      }

    public function relatorio_post_logs_normais(Request $request){
         $user = Auth::user();
//        $this->authorize('is_admin', $user);

         $data_inicial = $request->data_inicial != "" ? $request->data_inicial : "2010-01-01";
         $data_final = $request->data_final != "" ? $request->data_final : $date = date('Y-m-d');
         
        $componentes = DB::select(
            ' SELECT pacote_componente FROM `componentes` WHERE pacote_componente != ""'   // AS pacote
                );
        
        $order = $request['order'];
        $str_select =  ' SELECT * FROM `normal_logs` '   //, count(*) AS "c" //DATE_FORMAT(`datetime`, "%Y-%m-%d") AS `day`, AVG(cpuTime) AS ct, AVG(wallTime) AS wt
            . ' WHERE DATE_FORMAT(`datetime`, "%Y-%m-%d" ) >= ?'
            . ' AND DATE_FORMAT(`datetime`, "%Y-%m-%d" ) <= ?'
            . ' ORDER BY '.$order  ;
            
        
        if($request->pacote != "0"){
            $str_select .= " AND `executable` LIKE '" . $request->pacote . ".%'";
        } 

        $logs = DB::select( $str_select, [ $data_inicial, $data_final ] );             
        
        return view('relatorio.logs_normais', ['componentes'=>$componentes, 'logs'=>$logs, 
             ]);// json_encode($qtds)]);
    }
    
//  ANORMAIS
    public function relatorio_get_logs_anormais(){
        $user = Auth::user();
//        $this->authorize('is_admin', $user);

        $componentes = DB::select(
            ' SELECT pacote_componente FROM `componentes` WHERE pacote_componente != ""'   // AS pacote
                );
        
        $excecoes = DB::select(
            ' SELECT exception_name FROM `anormal_logs` '  // AS pacote     WHERE pacote_componente != ""' 
                );
          
        return view('relatorio.logs_anormais', ['componentes'=>$componentes, 'logs'=>[], 'excecoes'=>$excecoes,
                 ]);// json_encode($qtds)]);
      }
      
    public function relatorio_post_logs_anormais(Request $request){
        $user = Auth::user();
//        $this->authorize('is_admin', $user);

        $data_inicial = $request->data_inicial != "" ? $request->data_inicial : "2010-01-01";
        $data_final = $request->data_final != "" ? $request->data_final : $date = date('Y-m-d');
         
        $componentes = DB::select(
            ' SELECT pacote_componente FROM `componentes` WHERE pacote_componente != ""'   // AS pacote
                );
        
        $excecoes = DB::select(
            ' SELECT exception_name FROM `anormal_logs` '  // AS pacote     WHERE pacote_componente != ""' 
                );
        
        $order = $request['order'];
        $str_select =  ' SELECT * FROM `anormal_logs` '   //, count(*) AS "c" //DATE_FORMAT(`datetime`, "%Y-%m-%d") AS `day`, AVG(cpuTime) AS ct, AVG(wallTime) AS wt
            . ' WHERE DATE_FORMAT(`datetime`, "%Y-%m-%d" ) >= ?'
            . ' AND DATE_FORMAT(`datetime`, "%Y-%m-%d" ) <= ?'
            . ' ORDER BY '.$order  ;
            
//        ver como usar
        $titulo = "";
        if($request->pacote != "0"){
            $str_select .= " AND `executable` LIKE '" . $request->pacote . ".%'";
            $titulo .= $request->pacote . ', ';
        } 

        if($request->exception_name != "0"){
            $str_select .= " AND `exception_name` = '" . $request->exception_name . "'" ;
            $titulo .= $request->exception_name . ', ';
        }
        
        
        
        $logs = DB::select( $str_select, [ $data_inicial, $data_final ] );             
        
        return view('relatorio.logs_anormais', ['componentes'=>$componentes, 'logs'=>$logs, 'excecoes'=>$excecoes,
             ]);// json_encode($qtds)]);
    }

//    LOGS
//    public function relatorio_get_logs(){
//        $user = Auth::user();
////        $this->authorize('is_admin', $user);
//
//        $componentes = DB::select(
//            ' SELECT pacote_componente FROM `componentes` WHERE pacote_componente != ""'   // AS pacote
//                );
//          
//        return view('relatorio.logs', ['componentes'=>$componentes, 'logs_normais'=>[], 'logs_anormais'=>[], 
//                 ]);// json_encode($qtds)]);
//    }
    
}
