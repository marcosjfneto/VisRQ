<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use app\Entities\Pacote;
use Auth;
use DB;

class PacoteController extends Controller
{

    public function index() {
        $user = Auth::user();
        $this->authorize('is_admin', $user);
        
         $pacotes = Pacote::paginate(20);//where('ativo', 1)->
         return view('pacote.pacote_index', ['pacotes' => $pacotes]);
         
    }

    public function create()
    {
        $user = Auth::user();
        $this->authorize('is_admin', $user);
        
        return view('pacote.pacote_create');
    }
    
    public function store(Request $request)
    {
        $user = Auth::user();
        $this->authorize('is_admin', $user);
        
        Pacote::create([
        'nome'=> $request['nome'], 
        ]);
        
        \Session::flash('success','Pacote cadastrado!');
        return redirect()->route('pacote.index');
    }
    
   
    public function edit($id)
    {
        $user = Auth::user();
        $this->authorize('is_admin', $user);
        
        $pacote = Pacote::where('id', $id)
               ->get()[0];

        return view('pacote.pacote_edit', ['pacote' => $pacote]);
    }


    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $this->authorize('is_admin', $user);
        
        Pacote::where('id', $id)
            ->update([  'nome' => $request['nome'], 
                     ]);

        \Session::flash('success','Pacote alterado!');
        return redirect()->route('pacote.index');
    }
    
  
    public function destroy($id)
    {   
        $user = Auth::user();
        $this->authorize('is_admin', $user);
        
//        DB::update('update pacotes set ativo = ? where id = ?', [0, $id]);

        \Session::flash('success','Pacote excluído!');
        return redirect()->route('pacote.index');
    }
}
