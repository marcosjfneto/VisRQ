<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();


Route::group(['prefix'=>'usuarios', 'middleware'=>'auth', 'where'=>['id'=>'[0-9]+']], function() {
    Route::get('', ['as'=>'user.index', 'uses'=>'UserController@index']);

    Route::get('/criar', ['as'=>'user.create', 'uses'=>'UserController@create']);
    Route::post('/store', ['as'=>'user.store', 'uses'=>'UserController@store']);
    
    Route::get('/permissoes/{id}', ['as'=>'user.permissoes', 'uses'=>'UserController@permissoes']);
    Route::post('/permissoes/{id}', ['as'=>'user.permissoes_store', 'uses'=>'UserController@permissoes_store']);

    Route::post('/editar_funcionario', ['as'=>'user.edit_por_cpf', 'uses'=>'UserController@edit_por_cpf']);
    Route::get('/editar/{id}', ['as'=>'user.edit', 'uses'=>'UserController@edit']);
    Route::post('/update/{id}', ['as'=>'user.update', 'uses'=>'UserController@update']);
    
    Route::get('/editar_user/{id}', ['as'=>'user.edit_user', 'uses'=>'UserController@edit_user']);
    Route::post('/update_user/{id}', ['as'=>'user.update_user', 'uses'=>'UserController@update_user']);
    
    Route::get('/excluir/{id}', ['as'=>'user.destroy', 'uses'=>'UserController@destroy']);

});


//ADICIONADOS

Route::group(['prefix'=>'pacotes',  'where'=>['id'=>'[0-9]+']], function() {//'middleware'=>'auth',
    Route::get('/', ['as'=>'pacote.index', 'uses'=>'PacoteController@index']);
     
    Route::get('/cadastrar', ['as'=>'pacote.create', 'uses'=>'PacoteController@create']);
    Route::post('/cadastrar', ['as'=>'pacote.store', 'uses'=>'PacoteController@store']);
    
    Route::get('/editar/{id}', ['as'=>'pacote.edit', 'uses'=>'PacoteController@edit']);
    Route::post('/editar/{id}', ['as'=>'pacote.update', 'uses'=>'PacoteController@update']);
    
    Route::get('/excluir/{id}', ['as'=>'pacote.destroy', 'uses'=>'PacoteController@destroy']);
});

Route::group(['prefix'=>'logs',  'where'=>['id'=>'[0-9]+']], function() {
    
    Route::get('/Normais', ['as'=>'relatorio.get_logs_normais', 'uses'=>'RelatorioController@relatorio_get_logs_normais']);
    Route::post('/Normais', ['as'=>'relatorio.post_logs_normais', 'uses'=>'RelatorioController@relatorio_post_logs_normais']);
    
    Route::get('/Anormais', ['as'=>'relatorio.get_logs_anormais', 'uses'=>'RelatorioController@relatorio_get_logs_anormais']);
    Route::post('/Anormais', ['as'=>'relatorio.post_logs_anormais', 'uses'=>'RelatorioController@relatorio_post_logs_anormais']);
    
//    Route::get('/logs', ['as'=>'relatorio.get_logs', 'uses'=>'RelatorioController@relatorio_get_logs']);
    
});

Route::group(['prefix'=>'graficos',  'where'=>['id'=>'[0-9]+']], function() {
    //não é necessario utilizar essa rota
    Route::get('/relatorio1', ['as'=>'relatorio.index', 'uses'=>'RelatorioController@index']);
    
    Route::get('/resumo', ['as'=>'relatorio.get', 'uses'=>'RelatorioController@relatorio_get']);
    Route::post('/resumo', ['as'=>'relatorio.post', 'uses'=>'RelatorioController@relatorio_post']);
});

Route::group(['prefix'=>'arquitetura',  'where'=>['id'=>'[0-9]+']], function() {//'middleware'=>'auth',
    Route::get('/', ['as'=>'arquitetura.index', 'uses'=>'ArquiteturaController@index']);
     
    Route::get('/cadastrar_arquitetura', ['as'=>'arquitetura.create', 'uses'=>'ArquiteturaController@create']);
    Route::post('/cadastrar_arquitetura', ['as'=>'arquitetura.store_arquitetura', 'uses'=>'ArquiteturaController@store_arquitetura']);
    
    Route::post('/cadastrar_componente', ['as'=>'arquitetura.store_componente', 'uses'=>'ArquiteturaController@store_componente']);
    Route::post('/cadastrar_elemento', ['as'=>'arquitetura.store_elemento', 'uses'=>'ArquiteturaController@store_elemento']);
    
    Route::get('/importar', ['as'=>'arquitetura.importar_get', 'uses'=>'ArquiteturaController@importar_get']);
    Route::post('/importar', ['as'=>'arquitetura.importar_post', 'uses'=>'ArquiteturaController@importar_post']);
    
    Route::post('/importar_outro_diagrama', ['as'=>'arquitetura.importar_outro_diagrama_post', 'uses'=>'ArquiteturaController@importar_outro_diagrama_post']);
    
    Route::get('/componente/{id}', ['as'=>'arquitetura.show', 'uses'=>'ArquiteturaController@show']);
    
    Route::get('/editar/{id}', ['as'=>'arquitetura.edit', 'uses'=>'ArquiteturaController@edit']);
    Route::post('/editar/{id}', ['as'=>'arquitetura.update', 'uses'=>'ArquiteturaController@update']);
    
    Route::get('/excluir/{id}', ['as'=>'arquitetura.destroy', 'uses'=>'ArquiteturaController@destroy']);
});

Route::group(array('prefix' => 'api/v1'), function()
{   
    Route::resource('arquitetura', 'ArquiteturaController');//show
});
