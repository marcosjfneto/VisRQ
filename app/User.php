<?php

namespace app;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

    
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;//,  EntrustUserTrait; // add this trait to your user model
// na versão 5..2 do laravel EntrustUserTrait não deve ser usada pois fica conflitando e não permite fazer a relação
//    por isso, deve ser usado o método roles() para fazer o relacionamento.
    

    

    protected $table = 'users';


    protected $fillable = ['name','email','cpf', 'matricula', 'password', 'tipo'];


    protected $hidden = [ 'remember_token'];//'password',
    
    
//    public function roles()
//    {
//        return $this->belongsToMany('gerenciador\Entities\Role');
//    }
}
