<?php

namespace app\Entities;

use Illuminate\Database\Eloquent\Model;

class Pacote extends Model
{
    protected $table = 'pacotes';
    public $timestamps = false;
    protected $fillable = ['nome'];
}
