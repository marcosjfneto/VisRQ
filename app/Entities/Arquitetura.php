<?php

namespace app\Entities;

use Illuminate\Database\Eloquent\Model;

class Arquitetura extends Model
{
    protected $table = 'arquiteturas';
    public $timestamps = false;
    protected $fillable = ['nome'];
}
