<?php

namespace gerenciador\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }
    
    public function is_admin($user)
    {
        return $user->tipo == 'admin';
    }
    
    public function is_tec($user)
    {
        return $user->tipo == 'tec';
    }
    
    public function is_user($user)
    {
        return $user->tipo == 'user';
    }
    
    public function mesmo_user($user_logado, $user_passado)
    {
        return $user_logado->id == $user_passado->id;
    }
    
    public function is_user_or_tec($user)
    {
        return ($user->tipo == 'user') or ($user->tipo == 'tec');
    }
}
